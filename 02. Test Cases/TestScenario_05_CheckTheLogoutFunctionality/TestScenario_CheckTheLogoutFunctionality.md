## Test Scenario_Check the Logout Functionality 


### TC_05_01

***Title*** Logout - The link of logout is clickable

***Description*** Verify that link of logout is clickable when a user is logged-in

***Narrative*** In order to disconect with Healthy Food Social Network I want to logout 

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The user has logged in to the social network. The application is opened and it loads a home page and navigation bar with link to logout

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Click on "Logout" hyperlink


***Expected result*** Logout is clickable and user is logout social network  
-----------------

### TC_05_02

***Title*** Logout - The link of logout redirect to the login page

***Description*** Verify that logout page allow redirection to the page where it allows login

***Narrative*** In order to disconect with Healthy Food Social Network I want to logout 

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The user has logged in to the social network. The application is opened and it loads a home page and navigation bar with link to logout 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on "Logout" hyperlink


***Expected result*** The user has logged out of the social network. Page with fields for username and password loaded
----------------- 

### TC_05_03

***Title*** Logout - The functionality works correctly when using the "Back" button

***Description*** Verify by Click on logout s button, after successfully logout on login screen press back button

***Narrative*** In order to disconect with Healthy Food Social Network I want to logout 

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The user has logged in to the social network. The application is opened and it loads a home page and navigation bar with  link to logout
 
***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on "Logout" hyperlink
2. Press back buton 


***Expected result*** The user has logged out of the social network. The application is opened and it loads a home page and navigation bar
-----------------

### TC_05_04

***Title*** Logout - The functionality works correctly when reopening a closed tab

***Description*** Verify by Login with correct user close tab (x) and reopen (Chrome browser ctrl+Shift+ t) check user is logged in or auto logout from social network

***Narrative*** In order to disconect with Healthy Food Social Network I want to logout 

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The user has logged in to the social network. The application is opened and it loads a home page and navigation bar with link to logout 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Close the current tab
2. Navigate to https://still-retreat-67276.herokuapp.com


***Expected result*** The user is logged in the application
-----------------

### TC_05_05

***Title*** Logout - Check the logout button as a guest

***Description*** Verify Logout option not visible till the user is logged in

***Narrative*** In order to disconect with Healthy Food Social Network I want to logout 

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is opened and it loads a home page and navigation bar

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Ckeck for visibility of "Logout" hyperlink


***Expected result*** Logout option is not visible 
-----------------

### TC_05_06

***Title*** Logout - The functionality works correctly when using two or more browsers

***Description*** Verify when login in more than two browsers or mobiles and logout from any one from them and check all other account is proper working or all gets logout

***Narrative*** The user has logged in to the social network. The application is open in two browsers, Google Chrome and Firefox and loads a home page and navigation bar with link to logout

***Input Data*** https://still-retreat-67276.herokuapp.com, username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The user has logged in to the social network. The application is open in two browsers, Google Chrome and Firefox and loads a home page and navigation bar with link to logout
 
***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on "Logout" hyperlink in Chrome


***Expected result*** The user has logged out of the social network only from Chrome. The user remain logged in the social network in Firefox
-----------------