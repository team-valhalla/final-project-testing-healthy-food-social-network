## Test Scenario_Check Connect with people Functionality 
 
### TC_07_01

***Title*** Connect with people - List of all Public users

***Description*** Registered users should have interactions with other users. Register users should able to see a list with all public users and add friends by visiting their page

***Narrative*** As a user, I want to see a list with all public users and add friends

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Users from the Users'drop-down menu
3. Click on the More button on the bottom of the list to see the next page 


***Expected result*** Users able to see the page with a list of all public users
-------------------

### TC_07_02

***Title*** Connect with people - List of Connections

***Description*** Registered users should able to see the list with their friends 

***Narrative*** As a user, I want to see a list with my friends

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Friends from Users'drop-down menu
3. Click on the More button on the bottom of the list to see the next page 


***Expected result*** Users able to see the page with a list of their friends
-------------------

### TC_07_03

***Title*** Connect with people - Friendship requests notification

***Description*** Registered users should be able to receive notification about new friendship requests

***Narrative*** As a user, I want to check for friendship requests

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Check for notification about new friendship requests


***Expected result*** Users able to see a notification about new friendship requests in the Users section 
-------------------

### TC_07_04

***Title*** Connect with people - List with pending Friendship requests

***Description*** Registered users should be able to see a list with pending friendship requests 

***Narrative*** As a user, I want to check for friendship requests

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Requests from Users'drop-down menu
3. Click on the More button on the bottom of the list to see the next page 


***Expected result*** User able to see the page with unconfirmed friendship requests 
-------------------

### TC_07_05

***Title*** Connect with people - Send a friendship request

***Description*** While viewing another user profile, registered users should able to send a friendship request 

***Narrative*** As a user, I want to add friends

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Users from Users'drop down menu
3. Click on the More button on the bottom of the list to see the next page 
4. Choose a user you want to send a friendship request
5. Go to this person page by clicking his/her name 
4. To send a friendship request, click on the Connect button under their profile name


***Expected result*** Users able to see the Connect button changed with SENT REQUEST (REJECT) button
-------------------

### TC_07_06

***Title*** Connect with people - Accept a friendship request

***Description*** Registered users should be able to accept a friendship request

***Narrative*** As a user, I want to confirm a friendship requests

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and  loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Requests from Users'drop down menu
3. Click on the More button on the bottom of the list to see the next page
4. Choose a request you want to confirm
5. Go to this person page by clicking his/her name 
6. To confirm a friendship request, click on the Confirm button under the profile name


***Expected result*** Users able to see the Connect button changed with the DISCONNECT button. This person is visible on your friendship page 
----------------- 

### TC_07_07

***Title*** Connect with people - Unfriend existing friends

***Description*** Registered users should be able to unfriend any existing friend without approval

***Narrative*** As a user, I want to unfriend one of my existing friends

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** User has a registration and logged in. The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose Friends from Users'drop down menu
3. Click on the More button on the bottom of the list to see the next page
4. Choose a friend you want to unfriend
5. Go to this person page by clicking his/her name 
6. To unfriend user, click on the Disconnect button under the profile name


***Expected result*** Users able to see the Disconnect button changed with the CONNECT button. The person you unfriend is no more exist on your friendship page 
-----------------