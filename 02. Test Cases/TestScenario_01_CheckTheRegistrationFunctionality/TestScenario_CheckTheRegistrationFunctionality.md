## Test Scenario_Check the Registration Functionality 


### TC_01_01

***Title*** Registration - Sign Up option for new users

***Description*** Check whether the signup link for a new user is clickable and working as expected

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink

***Expected result*** Sign Up link navigates to the registration page. Registration page with required and optional fields is open 
-----------------

### TC_01_02

***Title*** Registration - Email confirmation

***Description*** Verify new users should get the email confirmation after the successful registration

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (Data: silmalirion111@gmail.com)
3. Enter age (Data: 50)
4. Enter password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name (Data: sully)
7. Enter the last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload it from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button
14. To finish your registration and before you can log in, you must activate your account by following the link sent to your email address
15. To confirm your email, click the link in the email you got when you created the account



***Expected result*** The user is registered, and a notification message is displayed. Page with fields for username and password is open 
-----------------

### TC_01_03

***Title*** Registration - Already registered email

***Description*** Check registration with already registered email

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter already registered email (Data: silmalirion111@gmail.com)
3. Enter age (Data: 50)
4. Enter password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name (Data: sully)
7. Enter the last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload a profile picture from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button


***Expected result*** User is not registered, and error message that User with the same username already exists displayed 
-----------------

### TC_01_04

***Title*** Registrtaion - Required fields

***Description*** Check system behavior by not filling any data in the required fields

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed  

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Do not fill any data in the username (email) field (Data:            )
3. Enter age (Data: 50)
4. Do not fill in any data in the password field (Data:            )
5. Do not fill in any data in the Confirm password field (Data:            )
6. Do not fill any data in the first name field (Data:            )
7. Do not fill any data in the last name field (Data:            )
8. Choose nationality 
9. Choose gender
10. Enter About your information 
11. Do not choose profile picture visibility
12. Do not select a profile picture (Data:            )
13. Click on the "Register" button


***Expected result*** The user is not registered, and an error message displayed that Fields marked with an * are required. 
------------------------

### TC_01_05

***Title*** Registration - User's first name to be 1 characters

***Description*** Check user should Register by filling all the required fields and User's first name to be 1 character

***Narrative*** In order to use Healthy Food Social Network I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open, it loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Enter age (Data: 50)
4. Enter password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name with 1 character (Data: s)
7. Enter the last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload a profile picture from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button


***Expected result*** The user is not registered and an error message is displayed, that Fields marked with an * are required and User's first name should be between 2 and 50 characters 
-----------------

### TC_01_06

***Title*** Registration - Password Validation 

***Description*** Check the password when passing invalid data

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Enter age (Data: 50)
4. Enter a password (Data: Valhalla)
5. Confirm a password (Data: Valhalla)
6. Enter a first name (Data: sully)
7. Enter last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload it from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button


*** Expected result *** The user is not registered, and an error message is displayed stating that the password must be at least 8 characters and contain at least 1 lowercase letter, 1 uppercase letter, 1 numeric character, and a special characters: [@, #, $,%,!]
-----------------
   
 ### TC_01_07

***Title*** Registration - Optional Fields

***Description*** Check all the optional fields when do not fill in data

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed  

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Do not enter the age
4. Enter a password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name (Data: sully)
7. Enter last name (Data: sully)
8. Do not choose nationality 
9. Do not select a gender
10. Do not enter About your information 
11. Choose profile picture visibility
12. Choose a profile picture and upload it from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button
14. To finish your registration and before you can log in, you must activate your account by following the link sent to your email address
15. To confirm your email, click the link in the email you got when you created the account


***Expected result*** The User is registered, and a notification message is displayed. Page with fields for username and password is open 
-----------------

### TC_01_08

***Title*** Registration - Negative value for age

*** Description *** Check the negative value for age

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Enter the age (Data: -100)
4. Enter a password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name (Data: sully)
7. Enter last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload it from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button


*** Expected result *** The user is not registered, and an error message displayed that the age must be positive
-----------------

### TC_01_09

***Title*** Registration - Too big value for age

***Description*** Check too big value for age

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed   

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Enter age (Data: 5555)
4. Enter a password (Data: Valhalla#1)
5. Confirm password (Data: Valhalla#1)
6. Enter the first name (Data: sully)
7. Enter last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender (optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload it from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button


*** Expected result *** The user is not registered and an error message is displayed stating that the age of the User must be between 15 and 100 numeric characters, for example.
-----------------

### TC_01_10

***Title*** Registration - Password Validation with different passwords

***Description*** Check registration with different passwords

***Narrative*** To use Healthy Food Social Network, I want to register 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed    

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Click on the "Sign Up" hyperlink
2. Enter username (email) (Data: silmalirion111@gmail.com)
3. Enter age (Data: 5555)
4. Enter a password (Data: Valhalla#1)
5. Enter a different password (Data: Valhalla#2)
6. Enter the first name (Data: sully)
7. Enter last name (Data: sully)
8. Choose nationality (optional)
9. Choose gender(optional)
10. Enter About your information (optional)
11. Choose profile picture visibility
12. Choose a profile picture and upload a profile picture from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
13. When the image is uploaded, click on the "Register" button 


*** Expected Result *** The user is not registered, and an error message is displayed stating that "Password does not match!"
-----------------