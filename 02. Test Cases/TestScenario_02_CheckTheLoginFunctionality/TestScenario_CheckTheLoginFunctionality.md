## Test Scenario_Check the Login Functionality 


### TC_02_01

***Title*** Login - Sign In Option

***Description*** Check whether the sign-in link is clickable and working as expected

***Narrative*** To use the social network for healthy food and connect with people, I want to log in 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and loads a home page and navigation bar with links to login and registration forms, profile search, and public feed 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign In" hyperlink


***Expected result*** SignIn link navigates to the login page. Page with fields for username and password is open 
-----------------

### TC_02_02

***Title*** User Login with a valid username and valid password

***Description*** Check system behavior and verify if a user will be able to login with a valid username and valid password 

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** The login page with the username and password fields is open 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Enter valid email (Data: silmalirion111@gmail.com)
2. Enter a valid password (Data: Valhalla#1)
3. Click on the "Log in" button


***Expected result*** User logged in. The application is open and  loads a navigation bar and a home page
-----------------

### TC_02_03

***Title*** User Login with a valid email and invalid password

***Description*** Check system behavior and verify if a user can log in when a valid email and invalid password entered

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** The login page with the username and password fields is open  

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Enter valid email (Data: silmalirion111@gmail.com)
2. Enter an invalid password (Data: Valhalla)
3. Click on the "Log in" button


***Expected result*** User not logged in to the application, and an error message is displayed
-----------------

### TC_02_04

***Title*** User Login with invalid email and valid password

***Description*** Check system behavior and verify if a user can log in when an invalid email and valid password is enter

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** The login page with the username and password fields is open 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Enter invalid email (Data: silmalirion@gmail.com)
2. Enter a valid password (Data: Valhalla#1)
3. Click on the "Log in" button


***Expected result*** User not logged in to the application, and an error message is displayed
-----------------

### TC_02_05

***Title*** User Login with forgot password link

***Description*** Verify Forgot Password sends a forgot password link, and the user can reset his/her password

***Narrative*** To connect with people, I want to reset my password for login

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** The login page with the username and password fields is open  

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the Forgot Password hyperlink
2. Enter valid email (Data: silmalirion111@gmail.com)
3. Click on the "Send" button
4. Check your inbox for the reset link 
5. Click the link in the email you got when you created the account
6. Page with fields for the new password is open
7. Enter a new password (Data: Valhalla#2)
8. Confirm new password (Data: Valhalla#2)
9. Click on the "Save" button
10. Login page with fields for username and password is open
11. Click on the "Log in" button


***Expected result*** The user logged in. The application is opened and loads a navigation bar and a home page
-----------------

### TC_02_06

***Title*** User Login with email and password fields left blank

***Description*** Check the behavior of the system when the email and password fields remain blank and the Login button pressed

***Narrative*** To use Healthy Food Social Network, I want to login 

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** Login page with fields for username and password is open 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Click on the "Sign In" hyperlink
2. Do not fill any data in fields for username and password (Data:        )
3. Click on the "Login" button


***Expected result*** User not registered, and an error message  displayed, that username or password is wrong  
------------------------

### TC_02_07

***Title*** User Login by ‘Enter’ key of the keyboard

***Description*** Verify if the ‘Enter’ key of the keyboard is working correctly on the login page

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** Login page with fields for username and password is open 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Enter valid email (Data: silmalirion111@gmail.com)
2. Enter a valid password (Data: Valhalla#2)
3. Click the ‘Enter’ key of the keyboard


***Expected result*** User logged in. The application is open and  loads a navigation bar and a home page
-----------------

### TC_02_08

***Title*** User Login - Messages for invalid login

***Description*** Verify the messages for invalid login

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** Page with fields for username and password is open 

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Enter invalid email (Data: silmalirion@gmail.com)
2. Enter an invalid password (Data: Valhalla#)
3. Click on the "Log in" button


***Expected result*** User not logged in to the application. Message for wrong username or password displayed. Recommended to be in red
-----------------

### TC_02_09

***Title*** User Login simultaneously in different browsers

***Description*** Verify if the login page allows to log in simultaneously in a different browser

***Narrative*** To connect with people, I want to login into Healthy Food Social Network

***Input Data*** https://still-retreat-67276.herokuapp.com/login

***Precondition*** Page with fields for username and password is open in two browsers simultaneously 

***Browser*** Chrome, Firefox

***Priority*** 1


***Steps to reproduce***
1. Enter the same valid email in both browsers (Data: silmalirion111@gmail.com)
2. Enter the same valid password in both browsers (Data: Valhalla#1)
3. Click on the "Log in" button in both browsers simultaneously


***Expected result*** User logged in both browsers. The application is open and loads a navigation bar and a home page
-----------------