## Test Scenario_Check Adminitration Functionality 


### TC_03_01

***Title*** The link of users is clickable when the admin is logged-in

***Description*** Verify that link of Users is visible and clickable when admin is logged-in

***Narrative*** As an admin I would like to see and modify the users profiles

***Input Data*** https://still-retreat-67276.herokuapp.com/  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads home page and navigation bar with link to Users

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Check Users hyperlink is visible
2. Click on the "Users" hyperlink

***Expected result*** Users hyperlink is visible and clickable and navigates to users's profiles page  
-----------------

### TC_03_02

***Title*** The link of Settings is visible when an admin is logged-in
 
***Description*** Verify that link of Settings is visible and drop down options clickable when admin is logged-in

***Narrative*** As an admin I would like to see and modify application options

***Input Data*** https://still-retreat-67276.herokuapp.com/  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads home page and navigation bar with link to Settings

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Check Settings hyperlink is visible
2. Click on the "Categories" hyperlink
3. Click on the "Nationalities" hyperlink
4. Click on the "Posts" hyperlink


***Expected result*** The Settings hyperlink is visible and drop-down options are clicked and navigated to the correct pages 
-----------------

### TC_03_03

***Title*** Еdit the user's profile as an admin

***Description*** The admin should be able to edit profiles information

***Narrative*** As admin I would like to edit profiles of the users and update names, age and optional information in case the information provided by the user is obscene, racist or contains any other false and unacceptable information

***Input Data*** https://still-retreat-67276.herokuapp.com/users  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Users. Page with Users is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Choose user profile to edit
2. Click on the user' name on which profile you want to edit 
3. Profile Page Opened. Click on the Edit button
4. Update names, age or optional information 
5. Click on the "Save" button


***Expected result*** The User's profile page is open and user profile updated
-----------------

### TC_03_04

***Title*** Еdit the user's profile picture and profile picture visibility as an admin

***Description*** The admin should be able to edit profiles pictures and profile picture visibility

***Narrative*** As admin I would like to edit profiles of the users and able to change their profile pictures and profile picture visibility in case the information provided by the user is obscene, racist or contains any other false and unacceptable information

***Input Data*** https://still-retreat-67276.herokuapp.com/users  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Users. Page with Users is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Choose user profile to edit
2. Click on the user's name on which profile you want to edit 
3. Profile Page Opened. Click on the Edit button
4. Change Profile picture visibility from the options menu just under the profile picture
5. Click on the Browse button in the right under your profile picture
5. Upload the picture you want from your computer
6. When the image is uploaded, click on Save 
5. Click the "Save" button


***Expected result*** The User's profile page is open and shows the updated profile photo, and is visible for the selected users
-----------------

### TC_03_05

***Title*** The admin should be able to see all users' profiles pictures

***Description*** The admin should be able to see all users's profiles pictures. It does not need to be connected to the user

***Narrative*** As an admin I would like to see all users's profiles pictures

***Input Data*** https://still-retreat-67276.herokuapp.com/users  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Users. Page with Users is open

***Browser*** Chrome

***Priority*** 2 


***Steps to reproduce***
1. Choose user profile to edit
2. Click on the user' name on which profile you want to edit 
3. Profile Page Opened. Click on the Edit button 
4. Update Profile picture visibility to connections 
5. Click on the "Save" button
6. Go to Users from the navigation bar
7. Choose Users from the drop down  s
8. Users' page opened. Find the updated user's profile and check profile photo visibility


***Expected result***  Updated user's profile picture to connections is visible for the admin 
-----------------

### TC_03_06

***Title*** An admin should be able to reset the user's password

***Description*** An admin should be able to reset a user's password by triggering an email to the user to allow them to log on and enter a new password, or generating a new password for the user and emailing it to the user 

***Narrative*** As an admin I want reset user's password

***Input Data*** https://still-retreat-67276.herokuapp.com/users  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it the loads home page and navigation bar with a link to Users. User's page is open

***Browser*** Chrome

***Priority*** 1


***Steps to reprоduce***
1. Click on the Change password button on the bottom of the page
2. Send an email to the user to allow them to log on and enter a new password
3. The user should get some notification that the admin reset their password
4. The system should not display the new password to the admin 
5. Admin should never be allowed to see the user's password, as some users may re-use passwords on multiple sites


***Expected result*** An email to reset and enter a new password has been sent to the user. The user successfully resets his password.
-----------------

### TC_03_07

***Title*** Delete user's profile as an admin

***Description*** The admin should be able to delete user profiles

***Narrative*** As an administrator, I would like to delete users who do not comply with the social network policy

***Input Data*** https://still-retreat-67276.herokuapp.com/users  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Users. Page with Users is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Choose user's profile to delete
2. Click on the user' name which profile you want to delete 
3. Profile Page Opened. Click on the Edit button
4. Click on the "Delete" button
5. You should confirm you want to delete the profile by Click "Yes" button


***Expected result*** The profile deleted. The Users page is open and updated
-----------------

### TC_03_08


***Title*** Edit users's posts as an admin

***Description*** The admin should be able to edit users's posts

***Narrative*** As admin i want to edit users's posts

***Input Data*** https://still-retreat-67276.herokuapp.com/admin/posts  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings. Page with Posts is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Choose a post to edit
2. Click on the post' name on which you want to edit 
3. Post Page Opened. Click on Edit the right under the post picture
4. Choose to update title, description, category and post picture 
5. Click on the "Save" button


***Expected result***  Post page is open and updated 
-----------------

### TC_03_09

***Title*** The admin should be able to see all posts

***Description*** The admin should be able to see all posts, public and connections. It does not need to be connected to the user

***Narrative*** As an admin I would like to see all posts

***Input Data*** https://still-retreat-67276.herokuapp.com/admin/posts  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings. Page with Posts is open

***Browser*** Chrome

***Priority*** 2 


***Steps to reproduce***
1. Choose a post to edit
2. Click on the post' name on which you want to edit 
3. Post Page Opened. Click on Edit the right under the post picture
4. Update post visibility to connections 
5. Click on the "Save" button
6. Go to Settings from the navigation bar
7. Choose Post from the drop down  
8. All Post page opened. Find the updated post


***Expected result***  Updated post to connections is visible for the admin 
-----------------

### TC_03_10

***Title*** Delete user's post as an admin 

***Description*** The administrator must be able to delete posts if they contain obscene, racist or other false and unacceptable information

***Narrative*** As an administrator, I would like to delete a post

***Input Data*** https://still-retreat-67276.herokuapp.com/admin/posts  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings. Page with All Posts is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Choose a post to delete
2. Click on the post' name which you want to delete 
3. Post Page is open. Click on Edit the right under the post picture
4. Click on the "Delete" button
5. You should confirm you want to delete the post by Click "Yes" button


***Expected result*** The post deleted. All Posts page is open and updated
-----------------

### TC_03_11

***Title*** Edit user's comment as an admin

***Description*** The admin should be able to edit post' comments 

***Narrative*** As an admin I want to edit post 'comments

***Input Data*** https://still-retreat-67276.herokuapp.com/admin/posts  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings. Page with All Posts is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Select the post whose comments you want to edit
2. Click on the post' name 
3. Post Page Opened. Click on Edit the right under the Comments section
4. Update the comment 
5. Click on the "Save" button 


***Expected result*** Post page is open. Comment updated
-----------------

### TC_03_12


***Title*** Delete user's comment as a user

***Description*** The admin should be able to delete post' comments

***Narrative*** As an admin I want to delete post 'comments

***Input Data*** https://still-retreat-67276.herokuapp.com/admin/posts  user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings. Page with All Posts is open

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Select the post whose comments you want to delete
2. Click on the post' name 
3. Post Page Opened. Click on Edit the right under the Comments section
4. Click on the "Delete" button 
5. You should confirm you want to delete the post' comment by Click "Yes" button


***Expected result*** Post page opened. The comment is not available
-----------------

### TC_03_13

***Title*** Add a category

***Description*** The admin should be able to add categories

***Narrative*** As an administrator, I would like to add a category

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it the loads home page and navigation bar with a link to Settings 

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Categories" from drop down
3. Click on the "Add" button on the top right on the screen
4. Enter Categoriy's name
5. Select and upload the Category's emoticon you want from your computer
6. When the image is uploaded, click on the Save button


***Expected result*** Categories page opened. The new Category added and visible
-----------------

### TC_03_14

***Title*** Add a category without an image

***Description*** The admin should not be able to add categories without image

***Narrative*** As an administrator, I would like to add a category

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Categories" from drop down
3. Click on "Add" button on the top right on the screen
4. Enter Categoriy's name
5. Do not upload emoticons
6. Click on the Save button


***Expected result***  The Category not added.An error message that "Category image is required" displayed
-----------------

### TC_03_15

***Title*** Edit a category

***Description*** The admin should be able edit categories 

***Narrative*** As an administrator, I would like to edit category

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Categories" from drop down
3. Cjoose category you want to edit and click on "Edit" button 
4. Edit Categoriy' name
5. Select and upload a new emoticon from your computer
6. When the image is uploaded, click on the Save button


***Expected result*** Categories page opened. The Category' name and emoticon updated
-----------------

### TC_03_16


***Title*** Delete a category

***Description*** The admin should be able to delete categories

***Narrative*** As an administrator, I would like to delete category

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it the loads home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Categories" from drop down
3. Choose category you want to delete and click on "Edit" button 
4. Click on the "Delete" button
5. You should confirm you want to delete the category by Click "Yes" button


***Expected result*** Categories page opened. The Category deleted
-----------------

### TC_03_17

***Title*** Add Nationality

***Description*** The admin should be able to add nationality

***Narrative*** As an administrator, I would like to add nationality

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Nationalities" from drop down
3. Click on the "Add" button on the top right on the screen
4. Enter Nationality' name
5. Click on the "Save" button


***Expected result*** Nationalities page opened. The Nationality added
-----------------

### TC_03_18

***Title*** Edit a nationality

***Description*** The admin should be able to edit nationality

***Narrative*** As an administrator, I would like to edit nationality

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2 


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Nationalities" from drop down
3. Choose nationality you want to edit and click on "Edit" button 
4. Edit the Nationality' name
5. Click on the "Save" button


***Expected result*** Nationalities page opened. The Nationality updated
-----------------

### TC_03_19

***Title*** Delete a nationality

***Description*** The admin should be able to delete nationality

***Narrative*** As an administrator, I would like to delete nationality

***Input Data*** https://still-retreat-67276.herokuapp.com user: valhalla.telerikacademy@gmail.com, password: Valhalla#1

***Precondition*** The admin is logged in. The application is open and it loads the home page and navigation bar with a link to Settings

***Browser*** Chrome

***Priority*** 2 


***Steps to reproduce***
1. Go to "Settings" from the navigation bar
2. Choose "Nationalities" from drop down
3. Choose nationality you want to delete and click on the "Edit" button 
4. Click on the "Delete" button
5. You should confirm you want to delete nationality by Click "Yes" button


***Expected result*** Nationalities page opened. The Nationality deleted
-----------------
