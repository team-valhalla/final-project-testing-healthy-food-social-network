## Test Scenario_Check Post Comment Functionality 


### TC_09_01

***Title*** Comment on an existing public post as а user

***Description*** Registered users should be able to comment on an existing public post

***Narrative*** As a registered user I want to comment public posts

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it the loads home page and navigation bar.the user is logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "All Post" hyperlink from drop-down
3. Choose Post you want to comment from the public feed
4. Click on that post' title
5. The post page is open. Write a comment in "Leave a Comment" text area (Data: "Hello")
6. Click on the "Send" button

***Expected result*** The comment is created. The comment is visible in the Comment section on the top right of the page
-----------------

### TC_09_02

***Title*** Comments on friends' timeline posts as а user

***Description*** Registered users should be able to comments on friends' timeline posts

***Narrative*** As a registered user I want to comments on my friends' timeline posts

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The user is logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "My Friends Posts" hyperlink from drop-down
3. Choose Post you want to comment from the friends' posts feed
4. Click on that post' title
5. The post page is open. Write a comment in the "Leave a Comment" text area (Data: "Hello")
6. Click on the "Send" button

***Expected result*** The comment is created. The comment is visible in the Comment section on the top right of the page
-----------------

### TC_09_03

***Title*** Post comment to the posts present on friend’s timeline

***Description*** Registered users should be able to post comments to the posts present in their friend’s timeline

***Narrative*** As a registered user I want to comment on the posts present in my friend’s timeline

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it the loads home page and navigation bar.the user is logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose and click on the "Friends" hyperlink from drop-down
3. Select a friend to whom you want to comment on a post from the friend's list
4. Click on that friend' name
5. The profile page is open. Go to "Lately post" just under the profile picture
6. Click on the "More" button at to bottom of the page to see all posts
7. Choose Post you want to comment and click on that post' title
8. The post page is open. Write a comment in the "Leave a Comment" text area
9. Click on the "Send" button

***Expected result*** The comment is created. The comment is visible in the Comment section on the top right of the page
-----------------

### TC_09_04

***Title*** Edit my own comment

***Description*** Registered users should be able to edit their comments 

***Narrative*** As a registered user, I want to edit my comment

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The post page is open

***Browser*** Chrome

***Priority*** 1

***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "My Friends Posts" hyperlink from drop-down
3. Choose the post where is the comment
4. Click on the "See All" button on the top right of the page to see all comments 
5. Choose from your personal comments to edit
6. Click on "Edit" 
7. Enter different text in the Comment text area (Data: "Bye")
8. Click on the "Save" button

***Expected result***  The comment is changed
-----------------

### TC_09_05

***Title*** Delete my own comment

***Description*** Registered users should be able to delete their comments

***Narrative***Registered users should be able to delete their comments

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The post page is open

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "My Friends Posts" hyperlink from drop-down
3. Choose the post where is the comment
4. Click on the "See All" button on the top right of the page to see all comments 
5. Choose from your personal comments to delete
6. Click on "Edit" 
7. Click on the "Delete" button
8. You should confirm you want to delete the post' comment by Click "Yes" button

***Expected result*** The comment is deleted
-----------------

### TC_09_06

***Title*** Add comment as a guest

***Description*** Anonymous users should not be able to comment on an existing public post

***Narrative*** As a anonymous user I do not want to be able to comment public posts

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and it loads the home page and navigation bar, with the link to public posts' s feed

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Click on the "Latest Posts" hyperlink 
3. Choose Post you want to comment from the public feed
4. Click on that post' title
5. The post page is open

***Expected result*** Anonymous users should not be able to comment and see the "Leave a Comment" section
-----------------

### TC_09_07

***Title*** Edit comment as a guest

***Description*** Anonymous users should not be able to edit comment on an existing public post

***Narrative*** As an anonymous user, I do not want to be able to edit comment on public posts

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The application is open and it loads the home page and navigation bar, with the link to public posts' s feed

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Click on the "Latest Posts" hyperlink 
3. Choose Post you want to comment from the public feed
4. Click on that post' title
5. The post page is open

***Expected result*** Anonymous users should not be able to edit the comment and see the "Edit" button 
-----------------