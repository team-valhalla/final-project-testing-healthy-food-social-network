## Test Scenario_Check Like Functionality 


### TC_10_01

***Title*** Like an existing public post as a user

***Description*** Registered users should be able to like an existing public post

***Narrative*** As a registered user I want to like public posts

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The user is logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "All Post" hyperlink from drop-down
3. Choose Post you want to like from the public feed
4. Click on that post' title
5. The post page is open. Click on the "Like" button under the post


***Expected result*** The post is liked. The color is changed. The number of likes is increased by 1. The text has changed  to "dislike"
-----------------

### TC_10_02

***Title*** Like on friends' timeline posts

***Description*** Registered users should be able to like on friends' timeline posts

***Narrative*** As a registered user I want to like my friends' timeline posts

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The user is logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "My Friends Posts" hyperlink from drop-down
3. Choose Post you want to like from the friends' posts feed
4. Click on that post' title
5. The post page is open. Click on the "Like" button under the post


***Expected result*** The post is liked. The color is changed. The number of likes is increased by 1. The text has changed  to "dislike"
-----------------

### TC_10_03

***Title*** Like posts present in my friend’s timeline

***Description*** Registered users should be able to like posts to the posts present in their friend’s timeline

***Narrative*** As a registered user I want to like post present in my friend’s timeline

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The user is logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Choose and click on the "Friends" hyperlink from drop-down
3. Select a friend to whom you want to like a post from the friend's list
4. Click on that friend' name
5. The profile page is open. Go to "Lately post" just under the profile picture
6. Click on the "More" button at to bottom of the page to see all posts
7. Choose Post you want to like and click on that post' title
8. The post page is open. Click on the "Like" button under the post


***Expected result*** The post is liked. The color is changed. The number of likes is increased by 1. The text has changed  to "dislike"
-----------------

### TC_10_04

***Title*** Unlike a post as a user

***Description*** Registered users should be able to unlike a post

***Narrative*** As a registered user I want to unlike post

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The post is already liked by the same user

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "All Post" hyperlink from drop-down
3. Choose Post you want to like from the public feed
4. Click on that post' title
5. The post page is open. Click on the "Dislike" button under the post


***Expected result*** The post is unliked. The color has changed. The number of counts is decreased by 1
-----------------

### TC_10_05

***Title*** Like a comment as a user

***Description*** Registered users should be able to like a comment

***Narrative*** As a registered user I want to like comments

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar.

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "All Post" hyperlink from drop-down
3. Choose a post
4. Click on that post' title
5. Click on the "See All" button on the top right of the page to see all comments 
6. Choose comment to like
7. Click on the "Like" button under the comment


***Expected result*** The comment is liked. The color was changed. The number of likes is increased by 1. The text has changed  to "dislike"
-----------------

### TC_10_06

***Title*** Unlike my comment as a user

***Description*** Registered users should be able to unlike a comment

***Narrative*** As a registered user I want to unlike the comment

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. The comment is already liked by the same user

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "All Post" hyperlink from drop-down
3. Choose a post
4. Click on that post' title
5. Click on the "See All" button on the top right of the page to see all comments 
6. Choose comment to like
7. Click on the "Dislike" button under the comment


***Expected result*** The comment is unliked. The color has changed. The number of likes is decreased by 1
-----------------

### TC_10_07

***Title*** Like a post as a guest

***Description*** Anonymous users should not be able to like a post

***Narrative*** As a guest I do not want to able to like a post

***Input Data*** https://still-retreat-67276.herokuapp.com/,  

***Precondition*** The application is open and it loads the home page and navigation bar

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce*** 
1. Go to the Latest Posts section from the navigation bar  
2. Click on the "Latest Posts" hyperlink 
3. Choose Post you want to like from the public feed
4. Click on that post' title
5. The post page is open


***Expected result*** "Like" button under the post not visible. You shouldn't be able to click the "Like" button
-----------------

### TC_10_08

***Title*** Like a comment as a guest

***Description*** Anonymous users should not be able to like a comment

***Narrative*** As a guest I do not want to able to like post's comments

***Input Data*** https://still-retreat-67276.herokuapp.com/,  

***Precondition*** The application is open and it loads the home page and navigation bar.

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar
2. Click on the "Latest Posts" hyperlink 
1. Click on the "See All" button on the top right of the page to see all comments 
2. All Comments page is open. Choose comment to like
3. Click the "Dislike" button under the comment


***Expected result***  "Like" button under the comment not visible. You shouldn't be able to click the "Like" button