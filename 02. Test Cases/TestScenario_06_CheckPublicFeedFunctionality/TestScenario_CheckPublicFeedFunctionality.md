## Test Scenario_Check Public feed Functionality     

### TC_06_01

***Title*** Public feed - Search profiles by name

***Description*** Anonymous users should be able to search Public profiles by first or family name. Check search result for “Exact Match” with the search keyword

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Type your friend's name into the search bar in the top right on the page and click on the Search button


***Expected result*** Person with the searched name exists on the public feed 
-----------------

### TC_06_02

***Title*** Public feed - Search profiles by email

***Description*** Anonymous users should be able to search Public profiles by email. Check search result for “Exact Match” with the search keyword

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Type your friend's email into the search bar in the top right on the page and click on the Search button


***Expected result*** Person with the searched email exists on the public feed 
-----------------

### TC_06_03

***Title*** Public feed - Search profiles by ‘Enter’ button on the keyboard

***Description*** Anonymous users should be able to search Public profiles when enters the keyword and hits the‘ Enter’ button on the keyboard

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Type your friend's name or email into the search bar in the top right on the page and hits the ‘Enter’ button on the keyboard


***Expected result*** Person with the searched name or email exists on the public feed 
-----------------

### TC_06_04

***Title*** Public feed - Search profiles when a user does not exist

***Description*** Validate search when the user does not exist

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Type your friend's name or email into the search bar in the top left right on the page and click on the Search button


***Expected result*** Error message that the searched user does not exist
-----------------

### TC_06_05

***Title*** Public feed - Search profiles by “Similar Match” 

***Description*** When an anonymous user starts typing a word in the text box it should suggest words that match the typed keyword. Check searched results for “Similar Match” with the search keyword

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Start typing a word into the search bar and see if suggest words that match 


***Expected result*** Search results displayed are relevant to the search keyword
-----------------

### TC_06_06

***Title*** Public feed - Search profiles with autocomplete 

***Description*** There should be pre-defined search criteria for auto-complete e.g. after typing the first 3 letters it should suggest matching keyword

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 4


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Typing 3 letter into the search bar and see if autocomplete suggest matches 


***Expected result*** Autocomplete after typing first 3 letters suggest matching
-----------------

### TC_06_07

***Title*** Search history

***Description*** After clicking Search field - search history should be displayed (latest search keyword)

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Click on the Search field 


***Expected result*** Search field left blank
-----------------

### TC_06_08

***Title*** Public feed - Search profiles with a set of keywords

***Description*** Check search with a set of keywords

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Type a set of keywords into the search bar 
4. Click on the Search button 


***Expected result*** Search results displayed are relevant to the searched set of keywords
-----------------

### TC_06_09

***Title*** Search results

***Description*** Total number of search results should be displayed 

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 


***Expected result*** Displayed the total number of searched results  
-----------------

### TC_06_10

***Title*** Post search and filtering 

***Description*** Check if the filtering of result work per option or is unresponsive for any specific option

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Click on the All Posts hyperlink 
3. Click on the Date button into the Sort bar on the right side of the page
4. Click on the Comments button into the Sort bar on the right side of the page
5. Click on the Like button into the Sort bar on the right side of the page


***Expected result*** Filtering works for any specific option
-----------------

### TC_06_11

***Title*** Post search and filtering with an empty search query

***Description*** Check if the filter option can be used with an empty search query

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on the Latest Posts hyperlink 
3. Left the search field blank in the search bar 
4. Click on the Date button into the Sort bar on the right side of the page


***Expected result*** Chronologically ordered public posts displayed
-----------------

### TC_06_12

***Title*** Post filtering before searching 

***Description*** Check if the application allows using a filter option before searching

***Narrative*** As an anonymous user, I want to search for friends

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on the Latest Posts hyperlink 
3. Click on the Comments button into the Sort bar on the right side of the page 
4. Type a word into the search bar 
5. Click on Search Button


***Expected result*** Filter option is possible before searching
-----------------

### TC_06_13

***Title*** Post filtering hidden

***Description*** Check if the filter option once applied can be hidden

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on the Latest Posts hyperlink  
3. Click on the Comments button into the Sort bar on the right side of the page
4. Click on the Comments button into the Sort bar on the right side of the page 


***Expected result*** Filter option once applied can be hidden
-----------------

### TC_06_14

***Title*** Notified results before Post filtering 

***Description*** Check how many results are notified before filter options are available

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on the Latest Posts hyperlink
3. Click on the Like button into the Sort bar on the right side of the page 


***Expected result*** The number of the results available is bigger before the filter option applied
-----------------

### TC_06_15

***Title*** Category filter 

***Description*** Check if Category filter options work as expected for any category

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on the Latest Posts hyperlink 
3. Click on sport hyperlink from Category sidebar 


***Expected result*** All posts from the sports category displayed 
-----------------

### TC_06_16

***Title*** Filter options for public profiles 

***Description*** Check if all the filter options for public profiles are available

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Search for friends from different nationality typing nationality into the search bar
4. Click on the search


***Expected result*** Only users from this nationality displayed on the screen
-----------------

### TC_06_17

***Title*** Options to clear the search query 

***Description*** Check if there are option/multiple options to clear the search query

***Narrative*** As an anonymous user, I want to search for friends 

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page

***Browser*** Chrome

***Priority*** 3


***Steps to reproduce***
1. Go to the Users section from the navigation bar  
2. Click on Users hyperlink 
3. Click on the Latest Posts hyperlink 
4. Click on sport hyperlink from Category sidebar
5. Try to find an option for clear and click on it


***Expected result*** Searched query cleared 
-----------------

### TC_06_18 

***Title*** Search specific symbols

***Description*** The user should be able to search specific symbols

***Narrative*** As a user I, want to be able to search specific symbols

***Input Data*** https://still-retreat-67276.herokuapp.com

***Precondition*** The application is open and loads a navigation bar and a home page with a link to Latest Posts. Post with "!@#$%" title created

***Browser*** Chrome

***Priority*** 3 


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Click on the All Posts hyperlink 
3. Type "!@#$%" in the search field 
4. Click the "Search" button 


***Expected result*** Should redirect to 404 ERROR
-----------------