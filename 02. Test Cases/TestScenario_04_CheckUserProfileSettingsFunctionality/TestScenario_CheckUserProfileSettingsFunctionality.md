## Test Scenario_Check User Profile Settings Functionality 


### TC_04_01

***Title*** Registered User profile settings 

***Description*** Verify that the link of the profile is visible and clickable when a user is logged-in

***Narrative*** As a registered user, I want to update my profile information 

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar with a link to edit the profile

***Browser*** Chrome

***Priority*** 1 


***Steps to reproduce***
1. Check Profile hyperlink is visible
2. Click on the "Profile" hyperlink


***Expected result*** Profile hyperlink is visible and clickable and navigates to user profile settings  
-----------------

### TC_04_02

***Title***  Registered User - Change profile name

***Description*** Registered user should be able to change their profile name

***Narrative*** As a registered user, I want to update my account name 

***Input Data*** https://still-retreat-67276.herokuapp.com/

*** Prerequisite *** The user is registered and logged in. The application is open and loads the homepage and navigation bar with a link to edit the profile

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, choose and click on Profile
3. Profile Page оpened. Click on the Edit button
4. Update the first name in the First Name text area (Data: sully)
5. Update the last name in the Last Name text area (Data: sully)
6. Click on Save to save the information


***Expected result*** The User profile page is open and the names updated
-----------------

### TC_04_03

***Title*** Registered User - Change profile picture

***Description*** Registered user should be able to change his/her profile picture 

***Narrative*** As a registered user, I want to change my profile picture  

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar, with a link to edit the profile

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, select and click on Profile
3. Profile Page opened. Click on the Edit button
4. Click on the Browse button in the right under your profile picture
5. Upload the image you want from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
6. When the image is uploaded, click on Save 


***Expected result*** The User profile page is open and shows the updated profile picture
-----------------

### TC_04_04

***Title*** Registered User - Change profile picture visibility to public

***Description*** Registered user should be able to change the visibility of their picture to Public

***Narrative*** As a registered user, I want to change the visibility of my profile picture to Public

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar with a link to edit the profile

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, select and click on Profile
3. Profile Page opened. Click on the Edit button
4. Choose to public Profile picture visibility from the options menu just under your profile picture
5. Click on Save 


***Expected result*** The User profile picture is public and visible for anonymous users and registered users
-----------------

### TC_04_05

***Title*** Registered User - Change profile picture visibility to connections

***Description*** Registered user should be able to change the visibility of their picture to Connections

***Narrative*** As a registered user, I want to change the visibility of my profile picture to Connections

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar with a link to edit the profile

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, select and click on Profile
3. Profile Page opened. Click on the Edit button
4. Choose to connections Profile picture visibility from the options menu just under your profile picture
5. Click on Save 


***Expected result*** The User profile picture is private and not visible to any other users
-----------------

### TC_04_06

***Title*** Registered User - Change account password

***Description*** Verify if registered user able to change his/her password

***Narrative*** As a registered user, I want to change my account password

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar with a link to edit the profile

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, select and click on Profile
3. Profile Page opened. Click on the Edit button
4. Click on the Change password button at the bottom of the page
5. Enter the valid old password (Data: Valhalla#1)
6. Enter new password (Data: Valhalla#3)
7. Confirm new password (Data: Valhalla#3)
5. Click on Save 


***Expected result*** User password is reset. Confirmation message displayed. The user is logged out and redirected to the Login page with fields for username and password 
-----------------

### TC_04_07

***Title*** Registered User - Change profile optional information

***Description*** Check if registered users can change any personal information such as age, nationality, gender, about you, etc

***Narrative*** As a registered user, I want to change the visibility of my profile picture to connections

***Input Data*** https://still-retreat-67276.herokuapp.com/

***Precondition*** The user has a registration and logged in. The application is open and loads the home page and navigation bar with a link to edit the profile.

***Browser*** Chrome

***Priority*** 2


***Steps to reprоduce***
1. Go to Profile from the navigation bar
2. From the drop-down, choose and click on Profile
3. Profile Page opened. Click on the Edit button
4. Edit the personal information - age, nationality, gender, about you, etc
5. Click on Save 


***Expected result*** The User profile page is open and shows the updated profile optional information
----------------- 