## Test Scenario 7: Check Post Functionality 


### TC_08_01

***Title*** Post - Create a public post 

***Description*** Registered user should be able to create a public post

***Narrative*** As a registered user I want to create a public post

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 1

***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "Hello")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
7. Set Post visibility to be public
8. Click on the "Save" button

***Expected result*** The post is created. The post is public and visible for registered and anonymous users
-----------------

### TC_08_02

***Title*** Post -  Create a private post

***Description*** Registered user should be able to create a private post

***Narrative*** As a registered user I want to create a post visible only to my connections

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "Hello")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer (Data: JPG File 11 kb, 185 x 185 pixels)
7. Set Post visibility to be connections
8. Click on the "Save" button


***Expected result*** The post is created. The post is private. Only my connections and admins can see it
-----------------

### TC_08_03

***Title*** Post - Create a post with video

***Description*** Registered user should be able to create a post with video

***Narrative*** As a registered user I want to create a post with a video

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "Hello")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the video you want from your computer (Data: MP4 File, 4.24MB, 1080x1920)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** The post is created
-----------------

### TC_08_04

***Title*** Post - Create a post without a title

***Description*** The registered user shouldn't be able to create a post without a title

***Narrative*** As a registered user I want all posts to contain a title

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Do not enter Post Title (Data: "Hello")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the video you want from your computer (Data: MP4 File, 4.24MB, 1080x1920)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** Post not created. Error message that "Post's tittle should be between 2 and 100 characters" displayed 
-----------------

### TC_08_05

***Title*** Post - Create a post without picture or video 

***Description*** The registered user shouldn't be able to create a post without a picture or video 

***Narrative*** As a registered user I want all my posts to contain picture or video

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "Hello")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Do not upload Post picture or video
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** Post not created. Error message that "Post image is required" displayed  
-----------------

### TC_08_06

***Title*** Post - Create a post without fill or choosing optional information

***Description*** The registered user should be able to create a post without fill or choosing optional information, as description or category

***Narrative*** As a registered user I want to create a post without fill or choosing optional information, as description or category

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "Hello")
4. Do not fill any data in the Description field
5. Do not choose any Category for your post 
6. Select and upload the image you want from your computer (Data: JPG File 11 kb, 185 x 185 pixels)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** The post is created 
-----------------

### TC_08_07

***Title*** Post - Create a post with a title of 1 symbols

***Description*** The registered user shouldn't be able to create a post with a short title

***Narrative*** As a registered user I do not want to be able to create a post with a title less than 2 symbols

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: "H")
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer  (Data: JPG File 15 kb, 185 x 185 pixels)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** Post not created. Error message that "Post's title should be between 2 and 100 characters" displayed 
-----------------

### TC_08_08

***Title*** Post - Create a post with a title of 2 symbols

***Description*** The registered user should be able to create a post with a title of 2 symbols

***Narrative*** As a registered user I want to able to create a post with a title of 2 symbols

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop down
3. Enter Post Title with 2 symbols
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer  (Data: JPG File 12 kb, 185 x 185 pixels)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** The post is created
-----------------

### TC_08_09

***Title*** Post - Create a post with a title more than 100 symbols

***Description*** The registered user shouldn't be able to create a post with a title more than 100 symbols

***Narrative*** As a registered user I do not want to able to create a post with a very long title

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop-down
3. Enter Post Title (Data: 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901)
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer (Data: JPG File 12 kb, 185 x 185 pixels)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** Post not created. Error message that "Post's title should be between 2 and 100 characters" displayed 
-----------------

### TC_08_10

***Title*** Post - Create a post with a title of 100 symbols

***Description*** The registered user should be able to create a post with a title of 100 symbols

***Narrative*** As a registered user I want to able to create a post with a title up to 100 symbols

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Choose and click on the "New Post" hyperlink from drop down
3. Enter Post Title with 100 symbols (Data: 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890)
4. Enter Post Description (optional)
5. Choose a Category for your post (optional)
6. Select and upload the post picture you want from your computer (Data: JPG File 18 kb, 185 x 185 pixels)
7. Set Post visibility 
8. Click on the "Save" button


***Expected result*** The post is created
-----------------

### TC_08_11

***Title*** Post - Edit own post

***Description*** The registered user should be able to edit his/her post

***Narrative*** As a registered user I want to able edit my posts

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Profile section from the navigation bar  
2. Choose and click on the "Profile" hyperlink from drop down
3. Choose Post you want to edit just under your profile picture
4. Click on that post' title
5. Click on Edit just under the post picture
6. Enter a post description (Data: "Hello")
7. Select and upload the video you want from your computer (Data: MP4 File, 4.24MB, 1080x1920)
8. Click on the "Save" button


***Expected result*** The post updated
-----------------

### TC_08_12

***Title*** Post - Delete own post

***Description*** The registered user should be able to delete his own post

***Narrative*** As a registered user I want to delete my post

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 2


***Steps to reproduce***
1. Go to the Profile section from the navigation bar  
2. Choose and click on the "Profile" hyperlink from drop-down
3. Choose Post you want to delete just under your profile picture
4. Click on that post' title
5. Click on Edit just under the post picture
6. Click on the "Delete" button
7. You should confirm you want to delete the post by Click "Yes" button


***Expected result*** The post deleted
-----------------

### TC_08_13

***Title*** Post - Cancel post deleting

***Description*** The registered user should be able to cancel post deleting

***Narrative*** As a registered user I want to cancel deleting my post

***Input Data*** https://still-retreat-67276.herokuapp.com/,  username: silmalirion111@gmail.com, password: Valhalla#1

***Precondition*** The application is open and it loads the home page and navigation bar. User logged in

***Browser*** Chrome

***Priority*** 1


***Steps to reproduce***
1. Go to the Profile section from the navigation bar  
2. Choose and click on the "Profile" hyperlink from drop-down
3. Choose Post you want to delete just under your profile picture
4. Click on that post' title
5. Click on Edit just under the post picture
6. Click on the "Delete" button
7. You can cancel delete the post by Click "No" button


***Expected result*** The post has not been deleted. It is still visible
-----------------

### TC_08_14
 
***Title*** Post - Create a post as a guest

***Description*** Anonymous users should not be able shouldn't be able to create a post
 
***Narrative*** As a guest I do not want to able to create a post

***Input Data*** https://still-retreat-67276.herokuapp.com/, 
 
***Precondition*** The application is open and it loads the home page and navigation bar, with a link to public posts' s feed
 
***Browser*** Chrome
 
***Priority*** 1

 
***Steps to reproduce***
1. Go to the Latest Posts section from the navigation bar  
2. Click on the "Latest Posts" hyperlink 
 
***Expected result*** Anonymous users shouldn’t be able to see and click on "New post"