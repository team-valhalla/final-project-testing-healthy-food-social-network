**TABLE OF CONTENTS**

- I. INTRODUCTION
- II. TEAM MEMBERS
- III. DELIVERABLES
- IV. USEFUL LINKS
- V. HOW TO RUN AUTOMATIONS


I. INTRODUCTION

HEALTHY FOOD SOCIAL NETWORK is a web application that allows you to connect and communicate with other people. It's inspired by Facebook and focuses on generating feed aimed at a healthy lifestyle.

The SOCIAL NETWORK application enables you to:

- Connect with people
- Create, comment and like posts
- Get a feed of the newest/most relevant posts of your connections 


II.  TEAM MEMBERS

QA Team:

- Dimitrina Murtova
- Daniel Radoslavov


III. DELIVERABLES 

- Software Requirement Specification	
- Master Test Plan version 1.12
- Test Cases	
- Exploratory Testing Summary Report	
- Test Reports_Manual Testing	
- UI Testing	
- API testing_POSTMAN	
- REST Assured Testing
- BDD	
- Final Test Report


Test Tools
- GitLab – for bug tracking
- Postman - for API testing
- REST Assured - for API testing
- Jython + Sikuli – for UI testing
- Selenium Web Driver + Java (POM) - for UI testing	
- Microsoft Visual Studio Code (VSC) – for test case management, for test scenarios, use cases and test cases creation
- GitLab – for source control 


IV. USEFUL LINKS

- Bug Tracking System - [GitLab](https://gitlab.com/team-valhalla/final-project-testing-healthy-food-social-network/-/issues)
- Full QA Documentation - [GitLab](https://gitlab.com/team-valhalla/final-project-testing-healthy-food-social-network) 
- QA Project Management Board - [Trello](https://trello.com/b/SRv90MGe/final-project-team-valhalla)


V. HOW TO RUN AUTOMATIONS

1. Pre-requisites for running automations

To be able to run the project on localhost and run the test automations, the following programs need to be present on your machine:

- Windows 10 operating system
- JDK 8
- Java 1.8
- IntelliJ IDEA Community Edition 
- Gradle Command Line
- MySQL Workbench  
- Postman standalone v7.10.0 or later
- Newman v4.5.5 or later
- Newman HTML Extra Reporter latest version
- Firefox latest version
- Selenium WebDriver
- Node.js v10.16.3 or later
- NPM v6.12.0 or later
- Apache Maven 3.6.0 or later
- Allure Command Line

2. Running the automation

- Clone the repo on your computer
- Run the .bat file in each folder






