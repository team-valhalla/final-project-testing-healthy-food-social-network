package com.telerikacademy.finalproject.models;

public class Connection {
    private long id;
    private UserDetails sender;
    private UserDetails receiver;
    private Status status;
}