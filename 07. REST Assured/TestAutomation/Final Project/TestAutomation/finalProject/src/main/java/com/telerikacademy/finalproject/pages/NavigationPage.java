package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage {

    public final String homeButton = "navigation.Home";
    public final String logOutButton = "navigation.LogOut";

    public NavigationPage() {
        super("base.url");
    }
}