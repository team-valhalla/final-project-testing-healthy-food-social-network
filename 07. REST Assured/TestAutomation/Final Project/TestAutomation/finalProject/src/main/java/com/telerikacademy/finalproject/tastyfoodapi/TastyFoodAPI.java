package com.telerikacademy.finalproject.tastyfoodapi;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.PropertiesManager;
import com.telerikacademy.finalproject.utils.RequestHandler;
import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import static com.telerikacademy.finalproject.utils.Utils.getConfigPropertyByKey;

public class TastyFoodAPI {


    public void authenticateDriverForUser(String usernameKey, String passwordKey, WebDriver driver) {
        String username = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(usernameKey);
        String password = PropertiesManager.PropertiesManagerEnum.INSTANCE.getConfigProperties().getProperty(passwordKey);
        driver.get(Utils.getConfigPropertyByKey("base.url"));
        driver.manage().addCookie(new Cookie("JSESSIONID", "emptyCookie", Utils.getConfigPropertyByKey("localhost:8080"), "/", null, true, true));

        // Authenticate and extract cookie
        RequestHandler client = new RequestHandler();
        String requestBody = "username=" + username + "&password=" + password;
        Response response = client.sendPostRequest("http://localhost:8080/authenticate", requestBody, ContentType.URLENC);
        String cookieValue = response.getDetailedCookie("JSESSIONID").getValue();
        //driver.manage().addCookie(new Cookie("JSESSIONID", cookieValue, Utils.getConfigPropertyByKey("localhost:8080"), "/", null, true, true));
        driver.manage().addCookie(new Cookie("JSESSIONID", cookieValue));
    }

}