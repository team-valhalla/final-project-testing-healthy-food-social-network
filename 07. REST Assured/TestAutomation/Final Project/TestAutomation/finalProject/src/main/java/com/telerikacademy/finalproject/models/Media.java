package com.telerikacademy.finalproject.models;

public class Media {
    private long id;
    private String picture;
    private MediaType mediaType;
    private Visibility visibility;
}