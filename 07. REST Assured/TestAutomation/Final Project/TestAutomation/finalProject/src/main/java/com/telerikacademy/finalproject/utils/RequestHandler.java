package com.telerikacademy.finalproject.utils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.openqa.selenium.Cookie;

import java.util.HashMap;

public class RequestHandler {
    public Response sendGetRequest(String endpoint, HashMap<String, String> queryParams){
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .queryParams(queryParams)
                .when()
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }

    public Response sendGetRequestWithoutParams(String endpoint){
        return RestAssured.given()
                .contentType(ContentType.JSON)
                .when()
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }

    public Response sendPostRequest(String endpoint, Object body, ContentType contentType, Cookie cookie){
        return RestAssured.given()
                .cookie(cookie.getName(), cookie.getValue())
                .contentType(contentType)
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .extract().response();
    }

    public Response sendPostRequest(String endpoint, Object body, ContentType contentType){
        return RestAssured.given()
                .contentType(contentType)
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .extract().response();
    }

    public Response sendPostRequestWithoutContentType(String endpoint, Object body){
        return RestAssured.given()
                .body(body)
                .when()
                .post(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }

    public Response sendPutRequest(String endpoint, int queryParam, Object body){
        return RestAssured.given()
                .queryParam(String.valueOf(queryParam))
                .when()
                .get(endpoint)
                .then()
                .statusCode(200)
                .extract().response();
    }
}