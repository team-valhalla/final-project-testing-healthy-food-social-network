package com.telerikacademy.finalproject.models;

import java.util.Set;

public class UserDetails {
    private long id;
    private String email;
    private String firstName;
    private String lastName;
    private Media picture;
    private String description;
    private int age;
    private boolean enabled;
    private Gender gender;
    private Nationality nationality;
    private Set<Post> posts;
}