package com.telerikacademy.finalproject.models;

import java.sql.Timestamp;
import java.util.Set;

public class Post {
    private long id;
    private Visibility visibility;
    private UserDetails creator;
    private String title;
    private String description;
    private Media media;
    private Timestamp timestamp;
    private Set<Category> categories;
    private Set<Comment> comments;
    private Set<UserDetails> likedUsers;
}