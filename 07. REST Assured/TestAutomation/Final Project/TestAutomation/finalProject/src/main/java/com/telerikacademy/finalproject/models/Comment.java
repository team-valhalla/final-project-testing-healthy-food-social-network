package com.telerikacademy.finalproject.models;

import java.sql.Timestamp;
import java.util.Set;

public class Comment {
    private long id;
    private Post post;
    private UserDetails creator;
    private String description;
    private Timestamp timestamp;
    private Set<UserDetails> likedUsers;
}