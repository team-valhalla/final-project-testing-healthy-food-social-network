package com.telerikacademy.finalproject.models;

import java.sql.Timestamp;

public class ConfirmationToken {
    private long id;
    private String confirmationToken;
    private Timestamp createdDate;
    private User user;
}