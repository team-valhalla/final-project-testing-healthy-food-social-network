package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.tastyfoodapi.TastyFoodAPI;
import com.telerikacademy.finalproject.utils.Utils;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.*;

import org.openqa.selenium.WebDriver;

public class HealthyFoodTestsAdminUser extends BaseTest{

    @Before
    public void navigateToHome_UsingNavigation(){

        tastyFoodAPI.authenticateDriverForUser("healthyFoodAdmin.username.encoded", "healthyFoodAdmin.pass.encoded", actions.getDriver());

        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
        actions.assertElementPresent(navPage.logOutButton);
    }

    @Test
    public void test_01_GET_listOfAllUserDetails() {

        given()
                .param("size", "10").
        when()
                .get(Utils.getUIMappingByKey("get.users.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_02_PUT_updateUser() {

        JSONObject request = new JSONObject();
        request.put("description", "Malkoto kote si e kato goliamoto, oba4e malko" );

        given()
                .cookie("JSESSIONID", "75C6DB9EBB25491FB69B95E5CF2FA5BC")
                .param("id", "5")
                .body(request.toJSONString()).
        when()
                .get(Utils.getUIMappingByKey("get.users.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_03_GET_listOfAllCategories() {

        given()
                .cookie("JSESSIONID", "75C6DB9EBB25491FB69B95E5CF2FA5BC")
                .
        when()
                .get(Utils.getUIMappingByKey("categories.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_04_GET_listOfAllNationalities() {

        given()
                .cookie("JSESSIONID", "75C6DB9EBB25491FB69B95E5CF2FA5BC")
                .
        when()
                .get(Utils.getUIMappingByKey("nationalities.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_05_createNationality() {

        JSONObject request = new JSONObject();
        request.put("id", "1" );
        request.put("nationality", "new" );

        given()
                .cookie("JSESSIONID", "75C6DB9EBB25491FB69B95E5CF2FA5BC")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(request.toJSONString()).
        when()
                .post(Utils.getUIMappingByKey("nationalities.url")).
        then()
                .statusCode(200)
                .log().all();
    }
}