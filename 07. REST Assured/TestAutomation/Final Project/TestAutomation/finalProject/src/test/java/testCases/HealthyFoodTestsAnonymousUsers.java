package testCases;

import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.RequestHandler;
import com.telerikacademy.finalproject.utils.Utils;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseBodyData;
import io.restassured.response.ResponseOptions;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.ANY;
import static io.restassured.http.ContentType.JSON;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import javax.tools.FileObject;
import java.util.HashMap;
import java.util.Map;

public class HealthyFoodTestsAnonymousUsers extends BaseTest {

    @Test
    public void test_01_GET_anonymosUserDetails() {

        given()
                .param("size", "10").
        when()
                .get(Utils.getUIMappingByKey("get.users.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_02_GET_listOfTop3posts() {

        given()
                .
        when()
                .get(Utils.getUIMappingByKey("get.top3posts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_03_GET_listOfPublicPostsSortedByDate() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("sort", "date");
        params.put("size", "10");

        given()
                .params(params).
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_04_GET_listOfListOfpublicPostsSortedByPage() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("sort", "date");
        params.put("page", "1");

        given()
                .params(params).
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_05_GET_searchPostByCategory() {

        given()
                .param("category", "2").
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_06_GET_searchPostByTitle() {

        given()
                .param("title", "treperete").
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_07_GET_filterPublicPostsByComments() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("sort", "comments");
        params.put("size", "10");

        given()
                .params(params).
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }

    @Test
    public void test_08_GET_filterPublicPostsByLikes() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("sort", "likes");
        params.put("size", "10");

        given()
                .params(params).
        when()
                .get(Utils.getUIMappingByKey("get.publicPosts.url")).
        then()
                .statusCode(200)
                .log().all();
    }
}