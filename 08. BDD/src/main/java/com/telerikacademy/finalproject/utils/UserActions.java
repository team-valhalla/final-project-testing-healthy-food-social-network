package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }


    public void goToHomePage() {
        driver.get("http://localhost:8080");
    }

    public void loginPage() {
        driver.get("http://localhost:8080/login");
    }

    public void logOut() {
        goToHomePage();
        waitAndClick("logOut");
    }

    //############# ELEMENT OPERATIONS #########

    public String lastComment() {
        String lastPostName = driver.findElement(By.xpath(Utils.getUIMappingByKey("comment.lastComment"))).getText();
        return lastPostName;
    }

    public void assertTextEquals(String expectedText, String realText) {
        Assert.assertEquals(expectedText, realText);
    }

    public void waitAndClick(String locator) {
        waitForElementVisible(locator, 500);
        clickElement(locator);
    }

    public String lastPostName() {
        String lastPostName = driver.findElement(By.xpath(Utils.getUIMappingByKey("postPage.lastMyPost"))).getText();
        return lastPostName;
    }

    public void uploadPostFile() {
        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("post.fileUpload")));
        uploadImage.sendKeys("D:\\test.jpg");
    }

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void clickDropDownElement(String locator) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
        Utils.LOG.info("Clicking on element " + locator);
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public void typeValueInFieldByConfProperty(String value, String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Utils.getConfigPropertyByKey(value));
    }

    public String getRandomString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public void uploadImage() {

        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("postPage.imageUpload")));
        uploadImage.sendKeys("D:\\test.jpg");
    }


    //############# WAITS #########

    public void waitForElementVisible(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }


    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    //############# ASSERTS #########

    public void assertWrongCredentials() {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey("wrongCredentials"))));
    }

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }
}
