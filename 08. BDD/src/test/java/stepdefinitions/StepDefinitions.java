package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();


    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field) {
        actions.typeValueInField(value, field);
    }

    @Given("I go to homepage")
    public void goToHomePage() {
        actions.goToHomePage();
    }


    @Given("I am on the Login page")
    public void goToLoginPage() {
        goToHomePage();
        actions.waitAndClick("signIn");
    }

    @When("I enter $userName and $userPassword")
    public void logInWithValidCredentials(String userName, String userPassword) {
        actions.typeValueInField(Utils.getConfigPropertyByKey(userName), "loginPage.usernameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey(userPassword), "loginPage.passwordField");
        actions.clickElement("loginPage.logInButton");
    }

    @When("When i create a post")
    public void createPost() {
        actions.clickDropDownElement("postPage.newPost");
        String title = "Hello,world";
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.typeValueInField(title, "postPage.description");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
    }

    @Then("Then the post is visible for community")
    public void postIsCreated() {
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals("Hello,world", lastPostName);
        actions.logOut();
        goToHomePage();
    }

    @When("I create a post $with title")
    public void createAPost(String with) {
        actions.clickDropDownElement("postPage.newPost");
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(Utils.getConfigPropertyByKey(with), "postPage.title");
        actions.typeValueInField("hoho", "postPage.description");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
    }

    @Then("I am redirected to the Home page")
    public void assertRedirectionToHomePage() {
        actions.waitForElementVisible("logOut", 5);
        actions.assertElementPresent("logOut");
        actions.logOut();
        goToHomePage();
    }

    @Then("I receive a $message")
    public void assertAlertMessageIsReceived(String message) {
        actions.assertElementPresent(message);
    }

    @Given("I am on the login page")
    public void goToRegistrationPage() {
        actions.loginPage();
    }

    @Given("I am login")
    public void logIn() {
        actions.clickElement("signIn");
        actions.waitForElementVisible("logInButton", 5);
        actions.typeValueInFieldByConfProperty("validUserName", "loginPage.usernameField");
        actions.typeValueInFieldByConfProperty("validUserPassword", "loginPage.passwordField");
        actions.clickElement("logInButton");
        actions.waitForElementVisible("profileButton", 5);
        actions.assertElementPresent("profileButton");
    }

    @When("I create comment on an existing public post")
    public void commentOnAnExistingPost() {
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastPost", 5);
        actions.clickElement("postPage.lastPost");
        String comment = "BDD is awesome";
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");
    }

    @Then("My comment is visible for community")
    public void commentIsCreated() {
        actions.waitForElementVisible("comment.lastComment", 5);
        String lastComment = actions.lastComment();
        actions.assertTextEquals("BDD is awesome", lastComment);
    }

    @Given("I am not login")
    public void iAmNotLogged() {
        actions.goToHomePage();
        actions.logOut();
    }

    @When("I am trying to comment a post")
    public void commentAsGuest() {
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastPost", 5);
        actions.clickElement("postPage.lastPost");
    }

    @Then("I am not able to comment")
    public void iAmNotAbleToComment() {
        actions.waitForElementVisible("comment.lastComment", 5);
        actions.assertElementNotPresent("comment.commentField");
    }
}
