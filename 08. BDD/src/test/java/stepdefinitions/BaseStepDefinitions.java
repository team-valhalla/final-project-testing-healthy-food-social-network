package stepdefinitions;

import com.telerikacademy.finalproject.utils.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;

public class BaseStepDefinitions {
    UserActions actions = new UserActions();
    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser("base.url");
    }

    @AfterStories
    public void afterStories(){
        actions.logOut();

        UserActions.quitDriver();
    }
}
