package testCases;

import com.telerikacademy.finalproject.tastyfoodapi.TastyFoodAPI;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

public class BaseTest {
	UserActions actions = new UserActions();
	TastyFoodAPI tastyFoodAPI = new TastyFoodAPI();
	protected WebDriver driver;

	@BeforeClass
	public static void setUp(){  UserActions.loadBrowser("base.url");	}

	@AfterClass
	public static void tearDown(){
				UserActions.quitDriver();
	}
}
