Meta:
@Post

Narrative:
As a registered user
I want to create a public post

Scenario:As a registered user I want to create a public post
Given I am login
When I create a post with title
Then Then the post is visible for community

Scenario:As a registered user I want all posts to contain a title
Given I am login
When I create a post without title
Then I receive a errorMessageForTitleValidation




