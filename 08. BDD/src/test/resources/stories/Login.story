Meta:
@logIn

Narrative:
As a registered user
I want to log in
So that I can get to the Home page.

Scenario:Check system behavior and verify if a user can log in when an invalid email and valid password is enter
Given I am on the Login page
When I enter invalidUserName and validUserPassword
Then I receive a wrongCredentialsMessage

Scenario:Check system behavior and verify if a user can log in when an valid email and invalid password is enter
Given I am on the Login page
When I enter validUserName and invalidUserPassword
Then I receive a wrongCredentialsMessage

Scenario:Check system behavior and verify if a user can log in when an invalid email and invalid password is enter
Given I am on the Login page
When I enter invalidUserName and invalidUserPassword
Then I receive a wrongCredentialsMessage


Scenario: To use the social network for healthy food and connect with people, I want to log in
Given I am on the Login page
When I enter validUserName and validUserPassword
Then I am redirected to the Home page




