Meta:
@Comment

Narative
As a registered user
I want to comment on an existing public post

Scenario: As a registered user I want to comment public posts
When I create comment on an existing public post
Then My comment is visible for community

Scenario: As a anonymous user I do not want to be able to comment public posts
Given: I am not login
When: I am trying to comment a post
Then: I am not able to comment
