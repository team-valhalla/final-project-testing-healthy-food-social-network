package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {
    public LoginPage() {
        super("login.url");
    }


    public void signInOption() {
        actions.clickElement("signIn");
        actions.assertElementPresent("usernameField");
        actions.assertElementPresent("passwordField");
    }

    public void login(String username, String password) {

        actions.typeValueInFieldByConfProperty(username, "usernameField");
        actions.typeValueInFieldByConfProperty(password, "passwordField");
        actions.clickElement("logInButton");

    }

    public void logInAsAUser() {
        actions.homePage();
        actions.clickTheElement("signIn");
        actions.waitForElementVisible("logInButton", 5);
        actions.typeValueInFieldByConfProperty("userName", "usernameField");
        actions.typeValueInFieldByConfProperty("userPassword", "passwordField");
        actions.clickElement("logInButton");
        actions.waitForElementVisible("profileButton", 5);
        actions.assertElementPresent("profileButton");
    }

    public void logInAsAnAdmin() {
        actions.homePage();
        actions.clickTheElement("signIn");
        actions.waitForElementVisible("logInButton", 5);
        actions.typeValueInFieldByConfProperty("adminName", "usernameField");
        actions.typeValueInFieldByConfProperty("adminPassword", "passwordField");
        actions.clickTheElement("logInButton");
        actions.waitForElementVisible("profileButton", 5);
    }


    //*********** Asserts *******************
    public void assertLoginPageIsLoadedCorrectly() {
        actions.assertElementPresent("usernameField");
        actions.assertElementPresent("passwordField");
    }

    public void assertWrongCredentials() {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey("wrongCredentials"))));

    }

    public void assertLoginSuccessful() {
        actions.waitForElementVisible("profileButton", 5);
        actions.assertElementPresent("profileButton");
    }

}
