package com.telerikacademy.finalproject.pages;

import org.junit.Before;

public class RegistrationPage extends BasePage {
    public RegistrationPage() {
        super("registration.url");
    }


    public void successfulRegistration() {
        actions.typeValueInField(actions.getRandomString(5) + "@abv.bg", "registerPage.usernameField");
        actions.clearAndWriteText("2", "registerPage.age");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.password");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.passwordConfirmation");
        actions.typeValueInField("Sully", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectNationality();
        actions.selectGender();
        actions.typeValueInField("Hello", "registerPage.aboutField");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");
    }

    public void alreadyRegisteredEmail() {
        actions.typeValueInFieldByConfProperty("userName", "registerPage.usernameField");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.password");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.passwordConfirmation");
        actions.typeValueInField("Sully", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");
    }

    public void requiredFields() {
        actions.clearAndWriteText("50", "registerPage.age");
        actions.selectNationality();
        actions.selectGender();
        actions.typeValueInField("Hello", "registerPage.aboutField");
        actions.clickElement("registerPage.visibilityMenu");
        actions.clickElement("registerPage.connections");
        actions.clickElement("registerPage.registerButton");
    }

    public void firstNameToBeOneCharacter() {
        actions.typeValueInFieldByConfProperty("userName", "registerPage.usernameField");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.password");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.passwordConfirmation");
        actions.typeValueInField("S", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");
    }

    public void passwordValidation() {

        actions.typeValueInFieldByConfProperty("userName", "registerPage.usernameField");
        actions.typeValueInField("Valhalla", "registerPage.password");
        actions.typeValueInField("Valhalla", "registerPage.passwordConfirmation");
        actions.typeValueInField("Sully", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");
    }

    public void negativeValueForAge() {

        actions.typeValueInFieldByConfProperty("userName", "registerPage.usernameField");
        actions.typeValueInField("-1", "registerPage.age");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.password");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.passwordConfirmation");
        actions.typeValueInField("Sully", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");

    }

    public void largeNumberForAge() {

        actions.typeValueInField(actions.getRandomString(5) + "@abv.bg", "registerPage.usernameField");
        actions.clickElement("registerPage.age");
        actions.typeValueInField("5555", "registerPage.age");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.password");
        actions.typeValueInFieldByConfProperty("userPassword", "registerPage.passwordConfirmation");
        actions.typeValueInField("Sully", "registerPage.firstName");
        actions.typeValueInField("Sully", "registerPage.lastName");
        actions.selectConnectionsVisibility();
        actions.uploadImage();
        actions.clickElement("registerPage.registerButton");

    }

    // ********** Asserts **************

    public void assertValidationMessage(String element) {
        actions.assertElementPresent(element);

    }


}
