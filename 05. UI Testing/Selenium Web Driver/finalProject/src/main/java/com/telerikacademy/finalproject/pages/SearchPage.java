package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.openqa.selenium.By;

public class SearchPage extends BasePage {
    public SearchPage(String urlKey) {
        super("users.url");
    }

    public void searchProfilesBy(String search) {

        actions.typeValueInFieldByConfProperty(search, "users.searchField");
        actions.clickTheElement("users.searchButton");
        actions.waitForElementVisible("users.moreButton", 5);

    }


    public void assertTheObjectIsFound(String locator) {
        actions.assertElementPresent(locator);
    }
}
