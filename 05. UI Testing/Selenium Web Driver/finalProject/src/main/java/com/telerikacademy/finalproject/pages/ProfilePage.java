package com.telerikacademy.finalproject.pages;

import org.junit.Assert;

public class ProfilePage extends BasePage {
    public ProfilePage(String urlKey) {
        super("profile.url");
    }

    public void changeUserFirstName(String firstName) {
        actions.clickElement("profile.edit");
        actions.clearAndWriteText(firstName, "profile.firstNameField");
        actions.clickElement("profile.saveButton");
        actions.waitForElementVisible("profile.firstName", 5);

    }

    public void changeUserLastName(String lastName) {

        actions.clickElement("profile.edit");
        actions.clearAndWriteText(lastName, "profile.lastNameField");
        actions.clickElement("profile.saveButton");
        actions.waitForElementVisible("profile.lastName", 5);

    }

    public void changeProfilePicture() {

        actions.waitForElementVisible("profile.imageLink", 5);
        actions.clickElement("profile.edit");
        actions.uploadImage();
        actions.clickElement("profile.saveButton");
        actions.waitForElementVisible("profile.imageLink", 5);

    }

    public void changeProfileOptionalInformation(String description, String age) {

        actions.clickElement("profile.edit");
        actions.clearAndWriteText(description, "profile.description");
        actions.clearAndWriteText(age, "profile.age");
        actions.clickElement("profile.saveButton");
        actions.waitForElementVisible("profile.lastName", 5);

    }

    public void editTheUsersProfileAsAnAdmin(String name) {

        actions.clickElement("profile.edit");
        actions.clearAndWriteText(name, "profile.firstNameField");
        actions.clickElement("profile.saveButton");


    }

    public void editTheUsersProfilePictureAsAnAdmin() {

        actions.clickElement("profile.edit");
        actions.uploadImage();
        actions.clickElement("profile.saveButton");

    }

    public void anAdminShouldBeAbleToResetTheUsersPassword() {

        actions.clickTheElement("profile.edit");
        actions.clickTheElement("profile.changePassword");
    }

//**********************************Asserts***********************************


    public void navigateToAUser() {
        driver.get("http://localhost:8080/users/6");
    }

    public void assertElementAreDifferent(String expectedText, String realText) {
        Assert.assertNotEquals(expectedText, realText);
    }

}
