package com.telerikacademy.finalproject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class NationalitiesPage extends BasePage {
    public NationalitiesPage(String urlKey) {
        super("Nationalities.url");
    }


    public void addNationality(String name) {

        actions.clickElement("nationalities.add");
        actions.clearAndWriteText(name, "nationalities.nameField");
        actions.clickElement("nationalities.saveButton");
        actions.waitForElementVisible("nationalities.add", 5);
    }

    public void deleteNationality() {
        selectLastNationalities();
        actions.clickTheElement("nationalities.deleteButton");
        actions.clickTheElement("nationalities.confirmDelete");
        actions.waitForElementVisible("nationalities.add", 5);

    }


    public void selectLastNationalities() {
        List<WebElement> allElement = driver.findElements(By.xpath("//a[@class='genric-btn success circle pull-right']"));
        int count = allElement.size();
        allElement.get(count - 1).click();
    }

}
