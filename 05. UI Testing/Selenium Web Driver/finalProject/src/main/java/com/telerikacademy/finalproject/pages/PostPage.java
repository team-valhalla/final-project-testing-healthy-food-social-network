package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.openqa.selenium.By;

public class PostPage extends BasePage {
    public PostPage(String urlKey) {
        super("posts.url");
    }


    public void createAPost(String title, String visibility) {
        actions.clickDropDownElement("postPage.newPost");
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadPostFile();
        actions.clickElement("postPage.visibility");
        actions.clickElement(visibility);
        actions.clickElement("postPage.saveButton");

    }

    public void createAPostWithVideo(String title) throws InterruptedException {

        actions.clickDropDownElement("postPage.newPost");
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadVideo();
        actions.clickElement("postPage.saveButton");
        Thread.sleep(7000);

    }

    public void createAPostWithoutPictureOrVideo(String title) {

        actions.clickDropDownElement("postPage.newPost");
        actions.waitForElementVisible(title, 5);
        actions.typeValueInField(title, "postPage.title");
        actions.clickElement("postPage.saveButton");
        actions.assertElementPresent("post.fileValidation");
    }

    public void createAPostWithoutFillOrChoosingOptionalInformation() {

        actions.clickDropDownElement("postPage.newPost");
        String title = actions.getRandomString(5);
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals(title, lastPostName);
    }

    public void createAPostWithATitleFfNSymbols() {

        actions.clickDropDownElement("postPage.newPost");
        String title = actions.getRandomString(1);
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
        actions.assertElementPresent("post.titleValidation");
    }

    public void deleteOwnPost() {
        actions.clickDropDownElement("postPage.newPost");
        String title = actions.getRandomString(5);
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals(title, lastPostName);
        actions.clickElement("postPage.lastMyPost");
        actions.clickElement("postPage.editPost");
        actions.clickElement("postPage.deleteButton");
        actions.clickElement("postPage.confirmDelete");
        actions.assertElementPresent("post.postIsDeleted");
    }

    public void editOwnPost() {
        actions.clickDropDownElement("postPage.newPost");
        String title = actions.getRandomString(5);
        actions.waitForElementVisible("postPage.title", 5);
        actions.typeValueInField(title, "postPage.title");
        actions.uploadPostFile();
        actions.clickElement("postPage.saveButton");
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals(title, lastPostName);
        actions.clickElement("postPage.lastMyPost");
        actions.clickElement("postPage.editPost");
        actions.clickElement("postPage.deleteButton");
        actions.clickElement("postPage.confirmDelete");
        actions.assertElementPresent("post.postIsDeleted");
    }

    public void commentOnAnExistingPublicPostAsAUser(String comment) {

        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastPost", 5);
        actions.clickElement("postPage.lastPost");
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");
        actions.waitForElementVisible("comment.lastComment", 5);
        String lastComment = lastComment();
        actions.assertTextEquals(comment, lastComment);
    }

    public void postCommentToThePostsPresentOnFriendTimeline(String comment) {

        actions.clickDropDownElement("friends");
        actions.waitForElementVisible("firstFriend", 5);
        actions.clickElement("firstFriend");
        actions.waitForElementVisible("lastFriendsPost", 5);
        actions.clickElement("lastFriendsPost");
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");

    }

    public void editMyOwnComment() {
        actions.clickDropDownElement("posts");
        actions.waitForElementVisible("postPage.lastPost", 5);
        actions.clickElement("postPage.lastPost");
        String comment = actions.getRandomString(5);
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");
        actions.waitForElementVisible("comment.lastComment", 5);
        actions.clickElement("comment.editLastComment");
        String newComment = actions.getRandomString(5);
        actions.typeValueInField(newComment, "comment.editCommentField");
        actions.clickElement("comment.editSaveButton");

    }

    public void deleteMyOwnComment() {
        actions.clickDropDownElement("posts");
        actions.waitForElementVisible("postPage.lastPost", 5);
        actions.clickElement("postPage.lastPost");
        String comment = actions.getRandomString(5);
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");
        actions.waitForElementVisible("comment.lastComment", 5);
        actions.clickElement("comment.editLastComment");
        actions.clickElement("comment.editDeleteButton");
        actions.clickElement("comment.editConfirmDelete");
        actions.assertElementPresent("comment.commentIsDeleted");
    }

    public void likeAnExistingPublicPostAsAUser() {
        actions.clickTheElement("postPage.likeButton");


    }

    public void dislikeAnExistingPublicPostAsAUser() {


        actions.clickTheElement("postPage.dislikeButton");


    }

    public void likeComment() {
        actions.clickTheElement("comment.likeButton");

    }

    public void dislikeComment() {
        actions.clickTheElement("comment.dislikeButton");

    }

    public void editUsersPostsAsAnAdmin() {
        actions.clickTheElement("postPage.editPost");
        String updatedTitle = actions.getRandomString(5);
        actions.clearAndWriteText(updatedTitle, "postPage.title");
        actions.clickTheElement("postPage.saveButton");
    }

    public void editTheUsersProfilePictureAsAnAdmin() {
        actions.navigateToAUser();
        String image = actions.imageLinkAddress();
        actions.clickElement("profile.edit");
        actions.uploadImage();
        actions.clickElement("profile.saveButton");
        String updatedImage = actions.imageLinkAddress();
        actions.assertTextNotEquals(image, updatedImage);
    }

    public void deleteUsersPostAsAdmin() {
        actions.clickTheElement("postPage.editPost");
        actions.clickTheElement("postPage.deleteButton");
        actions.clickTheElement("postPage.confirmDelete");
        actions.waitForElementVisible("comment.lastComment", 5);

    }

    public void editUsersCommentAsAnAdmin() {

        actions.clickTheElement("comment.editLastComment");
        String updateComment = actions.getRandomString(5);
        actions.clearAndWriteText(updateComment, "comment.commentField");
        actions.clickElement("comment.editSaveButton");

    }

    public void deleteUsersCommentAsAnAdmin() {
        actions.clickTheElement("comment.editLastComment");
        actions.clickElement("comment.editDeleteButton");
        actions.clickElement("comment.editConfirmDelete");

    }


    //************************ actions *************************

    public String lastComment() {
        String lastPostName = driver.findElement(By.xpath(Utils.getUIMappingByKey("comment.lastComment"))).getText();
        return lastPostName;
    }


    public void navigateToPost() {
        actions.clickTheElement("posts");
        actions.clickTheElement("postPage.lastPost");
    }


    public int postLikesCount() {
        int like = Integer.parseInt(driver.findElement(By.xpath(Utils.getUIMappingByKey("postPage.likesCount"))).getText());
        return like;
    }

    public int commentLikesCount() {
        int like = Integer.parseInt(driver.findElement(By.xpath(Utils.getUIMappingByKey("comment.likeCount"))).getText());
        return like;
    }

    public void likesAreEqual(int before, int after) {
        actions.assertNumbersAreEqual(before, after);
    }

    public void createAComment() {
        actions.clickTheElement("posts");
        actions.clickTheElement("postPage.lastPost");
        String comment = actions.getRandomString(5);
        actions.typeValueInField(comment, "comment.commentField");
        actions.clickElement("comment.commentSendButton");
        actions.waitForElementVisible("comment.lastComment", 5);
    }

    public void navigateToPostForEdit() {
        driver.get("http://localhost:8080/posts/38");
    }

    public void navigateTo(String locator) {
        String hero = Utils.getConfigPropertyByKey(locator);
        driver.get(hero);


    }

    //***************** Asserts **************************
    public void assertPostIsCreated(String title) {
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals(title, lastPostName);
    }

    public void assertPostDeletedFromAdmin() {
        navigateTo("postForDelete");
        actions.assertElementPresent("notFound");
    }

    public void assertPostWithVideoIsCreated(String title) {
        actions.clickElement("posts");
        actions.waitForElementVisible("postPage.lastMyPost", 5);
        String lastPostName = actions.lastPostName();
        actions.assertTextEquals(title, lastPostName);
        actions.clickElement("postPage.lastMyPost");
        actions.assertElementPresent("postPage.videoPresent");
    }

    public void assertValidation(String locator) {
        actions.assertElementPresent(locator);
    }

    public void assertPostIsDeleted() {
        actions.assertElementPresent("post.postIsDeleted");
    }

    public void assertPostEdited() {
        actions.assertElementPresent("post.postIsEdited");
    }

    public void assertCommentEdited() {
        actions.assertElementPresent("post.commentIsEdited");
    }

    public void assertCommentDeleted() {
        actions.assertElementPresent("post.commentIsDeleted");
    }

    public void assertCommentIsCreated(String comment) {
        actions.waitForElementVisible("comment.lastComment", 5);
        String lastComment = lastComment();
        actions.assertTextEquals(comment, lastComment);
    }

    public void assertLikesAreEqual(int before, int after) {
        actions.assertNumbersAreEqual(before, after);
    }

    public void assertTextIsEdited(String textOne, String textTwo) {
        actions.assertTextNotEquals(textOne, textTwo);
    }

    public void assertImagesAreDifferent(String imageOne, String imageTwo) {
        actions.assertTextNotEquals(imageOne, imageTwo);
    }


}
