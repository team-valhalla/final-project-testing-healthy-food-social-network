package com.telerikacademy.finalproject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CategoryPage extends BasePage {
    public CategoryPage(String urlKey) {
        super("categories.url");
    }

    public void addACategory(String newCategory) {
        actions.clickElement("categories.addCategory");
        actions.typeValueInField(newCategory, "categories.addField");
        actions.uploadCategoryImage();
        actions.clickElement("categories.saveButton");

    }

    public void addACategoryWithoutAnImage(String nameCategory) {
        actions.waitForElementVisible("categories.addCategory", 5);
        actions.clickElement("categories.addCategory");
        actions.typeValueInField(nameCategory, "categories.addField");
        actions.clickElement("categories.saveButton");
    }

    public void editACategory(String newName) {


        actions.clickTheElement("categories.aCategory");
        actions.clearAndWriteText(newName, "categories.addField");
        actions.clickElement("categories.saveButton");
        actions.waitForElementVisible("categories.name", 5);

    }

    public void deleteACategory() {

        actions.waitForElementVisible("categories.addCategory", 5);

        actions.selectLastCategory();
        actions.clickTheElement("categories.deleteButton");
        actions.clickTheElement("categories.confirmDelete");
        actions.waitForElementVisible("categories.addCategory", 5);

    }

    public void assertValidationMessage(String message) {
        actions.assertElementPresent(message);
    }

    public void selectLastCategory() {
        List<WebElement> allElement = driver.findElements(By.xpath("//a[@class='genric-btn success circle pull-right']"));
        int count = allElement.size();
        allElement.get(count - 1).click();
    }

}
