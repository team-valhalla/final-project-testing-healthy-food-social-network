package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class UserActions {


    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }


    //############# ELEMENT OPERATIONS #########

    public void clickTheElement(String locator) {
        waitForElementVisible(locator, 5);
        clickElement(locator);
    }


    public void clickBackButton() {
        driver.navigate().back();
    }




    public String copyLocatorText(String locator) {
        String text = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        return text;
    }

    public String imageLinkAddress() {
        WebElement img = driver.findElement(By.xpath("//img[@alt='profile picture']"));
        String src = img.getAttribute("src");
        return src;
    }

    public void clickDropDownElement(String locator) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
        Utils.LOG.info("Clicking on element " + locator);
    }

    public void clearAndWriteText(String value, String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(Keys.BACK_SPACE);
        element.sendKeys(value);
    }

    public void typeValueInFieldByConfProperty(String value, String field) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        element.sendKeys(Utils.getConfigPropertyByKey(value));
    }


    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }

    public String getRandomString(int n) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public int getRandomNumber() {
        Random number = new Random();
        int rand = number.nextInt(100);
        return rand;
    }

    public void logOut() {
        homePage();
        clickTheElement("logOut");
    }


    //############# WAITS #########

    public void waitForElementVisible(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));

    }


    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }


    public void uploadPostFile() {

        String pathToImage = System.getProperty("user.dir") + "\\images\\2.png";
        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("post.fileUpload")));
        uploadImage.sendKeys(pathToImage);
    }

    public void uploadVideo() {
        String pathToImage = System.getProperty("user.dir") + "\\images\\video.mp4";
        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("post.fileUpload")));
        uploadImage.sendKeys(pathToImage);
    }


    //############# ASSERTS #########


    public boolean greaterThenZero(int x) {
        if (x > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void assertElementNotPresent(String locator) {
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertTextEquals(String expectedText, String realText) {

        Assert.assertEquals(expectedText, realText);
    }

    public void assertNumbersAreEqual(int elementOn, int elementTwo) {
        Assert.assertTrue(elementOn == elementTwo);
    }
    public void textPresentOnPage(String text) {
        List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + text + "')]"));
        Assert.assertTrue("Text not found!", list.size() > 0);
    }

    public void assertTextNotEquals(String expectedText, String realText) {
        Assert.assertNotEquals(expectedText, realText);
    }

    public void assertOnExpectedPage(String url) {
        String title = driver.getTitle();
        Assert.assertEquals(Utils.getConfigPropertyByKey(url), title);

    }

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }


    public void loginPageIsOpenCorrectly() {
        assertOnExpectedPage("page.Login");
        assertElementPresent("usernameField");
        assertElementPresent("passwordField");

    }
    //############# userAction #########


    public void uploadImage() {
        String pathToImage = System.getProperty("user.dir") + "\\images\\2.png";
        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("postPage.imageUpload")));
        uploadImage.sendKeys(pathToImage);

    }

    public void uploadCategoryImage() {
        String pathToImage = System.getProperty("user.dir") + "\\images\\1.png";
        WebElement uploadImage = driver.findElement(By.xpath(Utils.getUIMappingByKey("categories.addImage")));
        uploadImage.sendKeys(pathToImage);
    }

    public int counter(String locator) {
        List<WebElement> like = driver.findElements(By.xpath(Utils.getUIMappingByKey(locator)));
        int likeSize = like.size();
        return likeSize;
    }


    public void selectLastCategory() {
        List<WebElement> allElement = driver.findElements(By.xpath("//a[@class='genric-btn success circle pull-right']"));
        int count = allElement.size();
        allElement.get(count - 1).click();
    }

    public void selectLastNationalities() {
        List<WebElement> allElement = driver.findElements(By.xpath("//a[@class='genric-btn success circle pull-right']"));
        int count = allElement.size();
        allElement.get(count - 1).click();
    }

    public void selectNationality() {
        clickElement("registerPage.nationalityMenu");
        clickElement("registerPage.nationality");
    }

    public void selectGender() {
        clickElement("registerPage.genderMenu");
        clickElement("registerPage.genderMale");
    }

    public void selectConnectionsVisibility() {
        clickElement("registerPage.visibilityMenu");
        clickElement("registerPage.connections");
    }

    public void assertWrongCredentials() {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey("wrongCredentials"))));
    }

    public String lastPostName() {
        String lastPostName = driver.findElement(By.xpath(Utils.getUIMappingByKey("postPage.lastMyPost"))).getText();
        return lastPostName;
    }

    public String userFirstName() {
        String firstName = driver.findElement(By.xpath(Utils.getUIMappingByKey("profile.firstName"))).getText();
        return firstName;
    }

    public String userLastName() {
        String firstName = driver.findElement(By.xpath(Utils.getUIMappingByKey("profile.lastName"))).getText();
        return firstName;
    }

    public String userAge() {
        String firstName = driver.findElement(By.xpath(Utils.getUIMappingByKey("user.age"))).getText();
        return firstName;
    }

    public String userAbout() {
        String firstName = driver.findElement(By.xpath(Utils.getUIMappingByKey("user.about"))).getText();
        return firstName;
    }
    //############# Navigation #########

    public void navigateToAUser() {
        driver.get("http://localhost:8080/users/6");
    }

    public void navigateToUserForDelete() {
        driver.get("http://localhost:8080/users/17");
    }

    public void navigateToFriendlyUser() {
        driver.get("http://localhost:8080/users/13");
    }

    public void navigateToPostForEdit() {
        driver.get("http://localhost:8080/posts/38");
    }

    public void navigateToPrivatePost() {
        driver.get("http://localhost:8080/posts/49");
    }



    public void navigateToPostWithComments() {
        driver.get("http://localhost:8080/posts/33");
    }

    public void homePage() {
        driver.get("http://localhost:8080");
    }
}
