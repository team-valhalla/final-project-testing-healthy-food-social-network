package testCases;

import com.telerikacademy.finalproject.pages.RegistrationPage;
import org.junit.Before;
import org.junit.Test;

public class RegistrationPageTests extends BaseTest {


    @Before
    public void navigateToTheLoginPage() {
        registerPage.navigateToPage();
    }

    @Test
    public void TC0102_SignUp_SuccessfulRegistration_When_RequiredFieldsAreFilled() {

        registerPage.successfulRegistration();


        registerPage.assertValidationMessage("page.SuccessRegistration");

    }

    @Test
    public void TC0103_SignUp_ErrorMessage_When_UseExistsEmail() {
        registerPage.alreadyRegisteredEmail();

        registerPage.assertValidationMessage("registerPage.ExistingEmail");
    }

    @Test
    public void TC0104_SignUp_ErrorMessage_When_DontFillInTheRequiredFields() {
        registerPage.requiredFields();

        registerPage.assertValidationMessage("registerPage.requiredFields");

    }

    @Test
    public void TC0105_SignUp_ErrorMessage_When_FillInOneCharacterAsFirstName() {
        registerPage.firstNameToBeOneCharacter();

        registerPage.assertValidationMessage("registerPage.nameValidation");
    }

    @Test
    public void TC0106_SignUp_ErrorMessage_When_FillInShortPassword() {
        registerPage.passwordValidation();

        registerPage.assertValidationMessage("registerPage.passValidation");
    }

    @Test
    public void TC0107_SigUp_ErrorMessage_When_FillInNegativeValueAsAge() {
        registerPage.negativeValueForAge();

        registerPage.assertValidationMessage("registerPage.agePositive");
    }

    @Test
    public void TC0108_SignUp_ErrorMessage_When_FillInLargeNumberAsAge() {
        registerPage.largeNumberForAge();

        registerPage.assertValidationMessage("registerPage.ageValidation");
    }


}
