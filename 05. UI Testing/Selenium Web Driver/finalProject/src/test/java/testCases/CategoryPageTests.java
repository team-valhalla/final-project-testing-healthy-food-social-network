package testCases;

import com.telerikacademy.finalproject.pages.CategoryPage;
import com.telerikacademy.finalproject.pages.LoginPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CategoryPageTests extends BaseTest {
    CategoryPage categoryPage = new CategoryPage("categories.url");
    LoginPage loginPage = new LoginPage();

    @Before
    public void loginAndNavigate() {
        loginPage.logInAsAnAdmin();
        categoryPage.navigateToPage();
    }

    @After
    public void logOut() {
        actions.logOut();
    }

    @Test
    public void TC0313_Add_ACategory_AsAnAdmin() {
        int size = actions.counter("categories.size");
        String newCategory = actions.getRandomString(5);

        categoryPage.addACategory(newCategory);

        int newSize = actions.counter("categories.size");
        actions.assertNumbersAreEqual(size + 1, newSize);
        actions.textPresentOnPage(newCategory);

    }

    @Test
    public void TC0314_Add_Category_WithoutAnImage_AsAnAdmin() {

        String newCategory = actions.getRandomString(5);

        categoryPage.addACategoryWithoutAnImage(newCategory);

        categoryPage.assertValidationMessage("imageIsRequired");
    }

    @Test
    public void TC0315_Add_Category_With_ShortName() {
        String newCategory = actions.getRandomString(1);

        categoryPage.addACategory(newCategory);

        categoryPage.assertValidationMessage("shortName");

    }

    @Test
    public void TC0316_Edit_Category_AsAnAdmin() {
        String name = actions.copyLocatorText("categories.name");
        String newName = actions.getRandomString(5);

        categoryPage.editACategory(newName);

        String updatedName = actions.copyLocatorText("categories.name");
        actions.assertTextNotEquals(name, updatedName);
    }

    @Test
    public void TC0317_Delete_Category_AsAnAdmin() {
        int size = actions.counter("categories.size");

        categoryPage.deleteACategory();

        int newSize = actions.counter("categories.size");
        actions.assertNumbersAreEqual(size - 1, newSize);
    }


}
