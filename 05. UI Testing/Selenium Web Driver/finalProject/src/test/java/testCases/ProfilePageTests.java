package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.ProfilePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProfilePageTests extends BaseTest {
    ProfilePage profilePage = new ProfilePage("profile.url");
    LoginPage loginPage = new LoginPage();

    @Before
    public void loginAndNavigate() {
        actions.homePage();
    }

    @After
    public void logOut() {
        actions.logOut();
    }

    @Test
    public void TC0402_Change_OwnFirstName_AsAUser() {
        loginPage.logInAsAUser();
        profilePage.navigateToPage();

        String userFirstName = actions.userFirstName();
        String firstName = actions.getRandomString(5);
        profilePage.changeUserFirstName(firstName);

        String currentFirstName = actions.userFirstName();
        profilePage.assertElementAreDifferent(currentFirstName, userFirstName);

    }

    @Test
    public void TC04021_Change_OwnLastName_AsAUser() {
        loginPage.logInAsAUser();
        profilePage.navigateToPage();

        String userLastName = actions.userLastName();
        String lastName = actions.getRandomString(5);
        profilePage.changeUserLastName(lastName);

        String currentLastName = actions.userLastName();
        profilePage.assertElementAreDifferent(currentLastName, userLastName);
    }

    @Test
    public void TC0403_Change_OwnProfilePicture_AsAUser() {
        loginPage.logInAsAUser();
        profilePage.navigateToPage();

        String UserImage = actions.imageLinkAddress();
        profilePage.changeProfilePicture();

        String currentUserImage = actions.imageLinkAddress();
        profilePage.assertElementAreDifferent(currentUserImage, UserImage);

    }

    @Test
    public void TC0407_Change_ProfileOptionalInformation_AsAUser() {
        loginPage.logInAsAUser();
        profilePage.navigateToPage();
        String userAge = actions.userAge();
        String userAbout = actions.userAbout();

        String description = actions.getRandomString(5);
        String age = Integer.toString(actions.getRandomNumber());
        profilePage.changeProfileOptionalInformation(description, age);

        String newUserAge = actions.userAge();
        String newUserAbout = actions.userAbout();
        actions.assertTextNotEquals(userAge, newUserAge);
        profilePage.assertElementAreDifferent(userAbout, newUserAbout);
    }

    @Test
    public void TC0303_Edit_TheUsersProfile_AsAnAdmin() {
        loginPage.logInAsAnAdmin();
        profilePage.navigateToAUser();

        String firstName = actions.userFirstName();
        String newName = actions.getRandomString(5);
        profilePage.editTheUsersProfileAsAnAdmin(newName);

        String updatedFirstName = actions.userFirstName();
        profilePage.assertElementAreDifferent(firstName, updatedFirstName);
    }

    @Test
    public void TC0304_Edit_TheUsersProfilePicture_AsAnAdmin() {
        loginPage.logInAsAnAdmin();
        profilePage.navigateToAUser();

        String image = actions.imageLinkAddress();
        profilePage.editTheUsersProfilePictureAsAnAdmin();

        String updatedImage = actions.imageLinkAddress();
        profilePage.assertElementAreDifferent(image, updatedImage);

    }

    @Test
    public void TC0305_TheAdmin_ShouldBeAbleToSee_AllUsersProfilesPictures() {
        profilePage.navigateToAUser();
        String image = actions.imageLinkAddress();

        loginPage.logInAsAnAdmin();
        profilePage.navigateToAUser();
        String imageVisibilityAsAdmin = actions.imageLinkAddress();

        profilePage.assertElementAreDifferent(image, imageVisibilityAsAdmin);
    }

    @Test
    public void TC0306_AnAdmin_ShouldBeAble_ToResetTheUsersPassword() {
        loginPage.logInAsAnAdmin();
        profilePage.navigateToAUser();
        String profileEmail = actions.copyLocatorText("users.emailAddress");

        profilePage.anAdminShouldBeAbleToResetTheUsersPassword();

        actions.assertOnExpectedPage("page.resetPassword");
        String pageEmail = actions.copyLocatorText("profile.currentEmailAddress");
        actions.assertTextEquals(profileEmail, pageEmail);
    }
}
