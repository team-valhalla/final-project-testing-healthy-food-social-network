package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.NationalitiesPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NationalitiesPageTests extends BaseTest {
    NationalitiesPage natPage = new NationalitiesPage("Nationalities.url");
    LoginPage loginPage = new LoginPage();

    @Before
    public void loginAndNavigateToThePage() {
        loginPage.logInAsAnAdmin();
        natPage.navigateToPage();
    }

    @After
    public void logOut() {
        actions.logOut();
    }


    @Test
    public void TC0318_Add_Nationality_AsAnAdmin() {

        int size = actions.counter("nationalities.size");
        String name = actions.getRandomString(5);
        natPage.addNationality(name);

        int newSize = actions.counter("nationalities.size");
        actions.assertNumbersAreEqual(size + 1, newSize);
    }

    @Test
    public void TC0319_Delete_Nationality_AsAnAdmin() {
        int size = actions.counter("nationalities.size");
        natPage.deleteNationality();

        int newSize = actions.counter("nationalities.size");
        actions.assertNumbersAreEqual(size - 1, newSize);


    }
}
