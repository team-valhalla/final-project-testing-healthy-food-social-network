package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LoginPageTests extends BaseTest {
    LoginPage loginPage = new LoginPage();

    @Before
    public void navigateToThePage() {
        loginPage.navigateToPage();
    }


    @Test
    public void TC0201_RequiredFields_AreLoadedCorrectly_InLoginPage() {
        loginPage.assertLoginPageIsLoadedCorrectly();
    }

    @Test
    public void TC0202_Login_Successful_When_EnterValidCredentials() {
        loginPage.logInAsAUser();

        loginPage.assertLoginSuccessful();

        actions.logOut();

    }

    @Test
    public void TC0203_Login_With_AValidEmailAndInvalidPassword() {
        loginPage.login("userName", "invalidPassword");

        loginPage.assertWrongCredentials();
    }

    @Test
    public void TC0204_Login_With_InvalidEmailAndValidPassword() {
        loginPage.login("invalidUsername", "userPassword");

        loginPage.assertWrongCredentials();
    }

    @Test
    public void TC0206_Login_With_EmailAndPassword_FieldsLeftBlank() {
        loginPage.login("", "");

        loginPage.assertWrongCredentials();
    }

    @Test
    public void TC0208_Login_With_InvalidEmailAndInvalidPassword() {
        loginPage.login("invalidUsername", "invalidPassword");

        loginPage.assertWrongCredentials();
    }

    @Test
    public void TC0209_Login_OnlyWithEmail_PasswordFieldLeftBlank() {
        loginPage.login("userName", "");

        loginPage.assertWrongCredentials();
    }

    @Test

    public void TC0210_Login_OnlyWithPassword_EmailFieldLeftBlank() {
        loginPage.login("", "userPassword");

        loginPage.assertWrongCredentials();
    }

}
