package testCases;

import com.telerikacademy.finalproject.pages.LoginPage;
import com.telerikacademy.finalproject.pages.PostPage;
import com.telerikacademy.finalproject.pages.ProfilePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PostPageTests extends BaseTest {

    PostPage postPage = new PostPage("post.url");
    LoginPage loginPage = new LoginPage();
    ProfilePage profilePage = new ProfilePage("profile.url");

    @Before
    public void navigateToThePage() {
        loginPage.navigateToPage();
    }

    @After
    public void logOut() {
        actions.logOut();
    }

    @Test
    public void TC0801_Create_APublicPost_AsAUser() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(5);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertPostIsCreated(title);

    }

    @Test
    public void TC0803_Create_APost_WithVideo() throws InterruptedException {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(5);
        postPage.createAPostWithVideo(title);

        postPage.assertPostWithVideoIsCreated(title);
    }

    @Test
    public void TC0804_ErrorMessage_When_CreateAPost_WithoutATitle() {
        loginPage.logInAsAUser();

        postPage.createAPost("", "postPage.visibilityPublic");

        postPage.assertValidation("post.titleValidation");
    }

    @Test
    public void TC0805_ErrorMessage_When_CreateAPost_WithoutAPictureOrVideo() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(5);
        postPage.createAPostWithoutPictureOrVideo(title);

        postPage.assertValidation("post.fileValidation");
    }

    @Test
    public void TC0806_Create_APost_Without_FillOrChoosingOptionalInformation() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(5);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertPostIsCreated(title);

    }

    @Test
    public void TC0807_ErrorMessage_When_CreateAPost_WithATitleOf1Symbols() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(1);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertValidation("post.titleValidation");
    }

    @Test
    public void TC0808_Create_APost_WithATitleOf2Symbols() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(2);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertPostIsCreated(title);
    }

    @Test
    public void TC0809_ErrorMessage_When_CreateAPost_WithATitleMoreThan100Symbols() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(101);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertValidation("post.titleValidation");
    }

    @Test
    public void TC0810_Create_APost_WithATitleOf100Symbols() {
        loginPage.logInAsAUser();

        String title = actions.getRandomString(100);
        postPage.createAPost(title, "postPage.visibilityPublic");

        postPage.assertPostIsCreated(title);
    }

    @Test
    public void TC0811_Delete_OwnPost_AsAUser() {
        loginPage.logInAsAUser();

        postPage.deleteOwnPost();

        postPage.assertPostIsDeleted();
    }

    @Test
    public void TC0811_Edit_OwnPost_AsAUser() {
        loginPage.logInAsAUser();

        postPage.editOwnPost();

        postPage.assertPostEdited();
    }

    @Test
    public void TC0901_Comment_OnAnExistingPublicPost_AsAUser() {
        loginPage.logInAsAUser();

        String comment = actions.getRandomString(5);
        postPage.commentOnAnExistingPublicPostAsAUser(comment);

        postPage.assertCommentIsCreated(comment);
    }

    @Test
    public void TC0902_Comment_OnFriendsTimelinePosts_AsAUser() {
        loginPage.logInAsAUser();

        String comment = actions.getRandomString(5);
        postPage.postCommentToThePostsPresentOnFriendTimeline(comment);

        postPage.assertCommentIsCreated(comment);
    }

    @Test
    public void TC0903_Comment_ToPosts_PresentOnFriendTimeline() {
        loginPage.logInAsAUser();

        String comment = actions.getRandomString(5);
        postPage.postCommentToThePostsPresentOnFriendTimeline(comment);

        postPage.assertCommentIsCreated(comment);
    }

    @Test
    public void TC0904_Edit_OwnComment_AsAUser() {
        loginPage.logInAsAUser();

        postPage.editMyOwnComment();

        postPage.assertCommentEdited();
    }

    @Test
    public void TC0905_Delete_OwnComment_AsAUser() {
        loginPage.logInAsAUser();

        postPage.deleteMyOwnComment();

        postPage.assertCommentDeleted();
    }

    @Test
    public void TC1001_Like_APost_AsAUser() {
        loginPage.logInAsAUser();
        postPage.navigateToPost();

        int likes = postPage.postLikesCount();
        postPage.likeAnExistingPublicPostAsAUser();
        int currentLikes = postPage.postLikesCount();


        postPage.likesAreEqual(likes + 1, currentLikes);

    }

    @Test
    public void TC102_Dislike_APost_AsAUser() {
        loginPage.logInAsAUser();
        postPage.navigateToPost();

        int likes = postPage.postLikesCount();
        postPage.dislikeAnExistingPublicPostAsAUser();
        int currentLikes = postPage.postLikesCount();

        postPage.assertLikesAreEqual(likes, currentLikes + 1);
    }

    @Test
    public void TC1001_LikeAndDislike_AnExistingPublicPost_AsAUser() {
        loginPage.logInAsAUser();
        postPage.createAComment();

        int likes = postPage.commentLikesCount();
        postPage.likeComment();
        int currentLikes = postPage.commentLikesCount();
        postPage.assertLikesAreEqual(likes + 1, currentLikes);

        postPage.dislikeComment();
        currentLikes = postPage.commentLikesCount();

        postPage.assertLikesAreEqual(likes, currentLikes);

    }

    @Test
    public void TC0308_Edit_UsersPosts_AsAnAdmin() {
        loginPage.logInAsAnAdmin();
        postPage.navigateTo("postForEdit");

        String title = actions.copyLocatorText("postPage.takeTitle");
        postPage.editUsersPostsAsAnAdmin();
        String currentTitle = actions.copyLocatorText("postPage.takeTitle");

        postPage.assertTextIsEdited(title, currentTitle);
    }

    @Test
    public void TC0309_TheAdmin_ShouldBeAble_ToSeeAllPosts() {
        postPage.navigateTo("postPrivate");
        actions.assertElementPresent("accessDenied");

        loginPage.logInAsAnAdmin();
        postPage.navigateTo("postPrivate");

        actions.assertElementNotPresent("accessDenied");

    }

    @Test
    public void TC0310_Delete_UsersPost_AsAdmin() {
        loginPage.logInAsAnAdmin();
        postPage.navigateTo("postForDelete");

        postPage.deleteUsersPostAsAdmin();

        postPage.assertPostDeletedFromAdmin();
    }

    @Test
    public void TC0311_Edit_UsersComment_AsAnAdmin() {
        loginPage.logInAsAnAdmin();
        postPage.navigateTo("postWithComments");

        String lastComment = actions.copyLocatorText("comment.lastComment");
        postPage.editUsersCommentAsAnAdmin();
        String currentComment = actions.copyLocatorText("comment.lastComment");

        actions.assertTextNotEquals(lastComment, currentComment);
    }

    @Test
    public void TC0312_Delete_UsersComment_AsAnUser() {
        loginPage.logInAsAnAdmin();
        postPage.navigateTo("postWithComments");

        String lastComment = actions.copyLocatorText("comment.lastComment");
        postPage.deleteUsersCommentAsAnAdmin();
        String currentComment = actions.copyLocatorText("comment.lastComment");

        actions.assertTextNotEquals(lastComment, currentComment);

    }


}
