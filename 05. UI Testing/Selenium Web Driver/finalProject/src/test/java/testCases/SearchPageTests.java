package testCases;

import com.telerikacademy.finalproject.pages.SearchPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SearchPageTests extends BaseTest {

    SearchPage searchPage = new SearchPage("users.url");

    @Before
    public void loginAndNavigateToThePage() {
        searchPage.navigateToPage();
    }

    @Test
    public void TC0601_Search_ProfilesByFirstName_AsAGuest() {
        searchPage.searchProfilesBy("userFirstName");

        searchPage.assertTheObjectIsFound("searchedName");

    }

    @Test
    public void TC0602_Search_ProfilesByFullName_AsAGuest() {
        searchPage.searchProfilesBy("userFullName");

        searchPage.assertTheObjectIsFound("searchedName");
    }

    @Test
    public void TC0603_Search_ProfilesByEmail_AsAGuest() {
        searchPage.searchProfilesBy("adminName");

        searchPage.assertTheObjectIsFound("searchedName");
    }

    @Test
    public void TC0608_Search_ProfilesWithASetOfKeywords_AsAGuest() {
        searchPage.searchProfilesBy("c@ma");

        searchPage.assertTheObjectIsFound("searchedName");
    }

    @Test
    public void TC0618_Search_SpecificSymbols_AsAGuest() {
        searchPage.searchProfilesBy("!@#$%");

        actions.assertElementNotPresent("error404");

    }

}
