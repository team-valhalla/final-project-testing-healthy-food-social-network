package testCases;

import com.telerikacademy.finalproject.pages.RegistrationPage;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {

	RegistrationPage registerPage = new RegistrationPage();
	UserActions actions = new UserActions();

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("base.url");
	}

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}

	public UserActions getActions() {
		return actions;
	}

}

