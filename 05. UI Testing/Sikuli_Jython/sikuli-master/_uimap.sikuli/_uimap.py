########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class Windows:
    TextField = "TextField.png"
    Search = "WinSearch.png"

class HomePage:
    SignIn = "Sign.png"

class LoginPage:
    LoginForm = "LoginForm.png"

class MainPageNavBar:
    Settings = "Settings.png"  
    Users = "FriendshipRequest.png"  

class Categories:  
    Categories = "Categories.png"  
    AddCategory = "AddCategory.png" 
    SelectFile = "SelectFile.png"    
    Levski = "_uimap.sikuli\LEVSKI.png"     
    Open = "Open.png"
    Error = "Error.png"
    Edit = "EditLevski.png"
    Delete = "Delete.png"
    Yes = "Yes.png"

class Nationalities:
    Nationalities = "Nationalities.png"
    AddNationality = "AddNationality.png"
    TypeNationality = "TypeNationality.png"
    EditVal = "EditVal.png"
    EditValhalla = "EditValhalla.png"
    Delete = "Delete.png"
    Yes = "Yes.png"

class Posts: 
    SearchPost = "SearchPost.png"
    SearchFieldPost = "SearchFieldPost.png"
    SearchButton = "SearchButton.png"
    Slavia = "Slavia.png"
    SlaviaPostTitle = "SlaviaPostTitle.png"
    ValhallaHero = "ValhallaHero.png"
    ValhallaClickPostTitle = "ValhallaClickPostTitle.png"
    EditPost = "EditPost.png"
    Description = "Description.png"
    DeletePost = "DeletePost.png"
    NoDelete = "No.png"
    SavePost = "SavePost.png"
    PostExists = "PostExists.png"
    PostToDelete = "PostToDelete.png"
    PostTitleToDelete = "PostTitleToDelete.png"
    PostPresent = "PostPresent.png"

class Users: 
    AllUsers = "Users.png"
    Profile = "Profile.png"
    More = "More.png"
    SearchFieldUser = "SearchFieldUsers.png"
    SearchUser = "SearchUser.png"
    LokiUser = "Loki.png"
    LokiTitle = "lokiTitle.png"

class Connections:
    Friends = "Friends.png"
    Requests = "REQUESTS.png"
    RequestsVisible = "RequestsVisible.png"
    Connect = "Connect.png"
    UserProfile = "VENTCITUMBAK.png"
    SendRequestReject = "SendRequest.png"
    ConfirmRequest = "ConfirmRequest.png"
    Disconnect = "VENTCITUMBAK-1.png"
    More = "More.png"

class NewPost: 
    NewPost = "NewPost.png"
    MyFriendsPosts = "MyFriendsPosts.png"
    Like = "Like.png"   
    Keyword = "KeywordForPostSearch.png"
    LeaveComment = "LeaveComment.png"
    SendComment = "SendComment.png"
    More = "More.png"
    UserBBCC = "userBBCC.png"
    HelloThere = "HelloThere.png"
    HelloThereTitlePost = "HelloThereTitlePost.png"                        