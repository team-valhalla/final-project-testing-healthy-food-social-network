from sikuli import *
import unittest
import iHTMLTestRunner.iHTMLTestRunner as HTMLTestRunner
from _uimap import *

class SocialNetwork:
	@classmethod
	def Start(self):
		type('r', Key.WIN)
		sleep(2)
		type('chrome /incognito')
		sleep(2)
		type(Key.ENTER)
		sleep(1)
	
	@classmethod
	def Close(self):
		type(Key.F4, Key.ALT)