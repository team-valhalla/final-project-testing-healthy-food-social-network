from _lib import *

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_001_OpenWindows(self):
        SocialNetwork.Start()

    def test_002_Search(self):
        click(Windows.TextField)       
        type('http://localhost:8080/')
        type(Key.ENTER) 
        wait(HomePage.SignIn,5) 
        assert(exists(HomePage.SignIn))

    def test_003_UserSignIn(self):        
        click(HomePage.SignIn)  
        wait(5)          
        type(Key.TAB)
        type('silmalirion111@gmail.com')
        type(Key.TAB)
        type('Valhalla#1')
        type(Key.ENTER)  
        wait(Users.Profile,5) 
        assert(exists(Users.Profile).highlight(2))

    def test_004_SearchForFriends(self):
        click(Users.AllUsers)
        wait(Users.SearchFieldUser,5)  
        click(Users.SearchFieldUser)
        type('loki')
        click(Users.SearchUser) 
        assert(exists(Users.LokiUser).highlight(2))

    def test_005_SendFriendshipRequest(self):
        click(Users.LokiTitle)
        wait(Connections.Connect,5)  
        click(Connections.Connect)
        wait(Connections.SendRequestReject,5) 
        assert(exists(Connections.SendRequestReject).highlight(2))  

    def test_006_AcceptFriendshipRequest(self):
        waitVanish(MainPageNavBar.Users)
        hover(MainPageNavBar.Users)
        wait(Connections.Requests,5)  
        click(Connections.Requests)
        wait(Connections.RequestsVisible,5)
        click(Connections.UserProfile)
        wait(Connections.ConfirmRequest,5)
        click(Connections.ConfirmRequest)
        assert(exists(Connections.Disconnect).highlight(2))

    def test_015_CloseWindows(self):
        SocialNetwork.Close()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='Registered Users Tests Report' )
    runner.run(suite)
    outfile.close()

