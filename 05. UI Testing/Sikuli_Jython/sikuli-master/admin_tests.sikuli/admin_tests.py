from _lib import *

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_001_OpenWindows(self):
        SocialNetwork.Start()

    def test_002_Search(self):
        click(Windows.TextField)       
        type('http://localhost:8080/')
        type(Key.ENTER) 
        wait(HomePage.SignIn,5) 
        assert(exists(HomePage.SignIn))
        
    def test_003_AdminSignIn(self):               
        click(HomePage.SignIn)  
        wait(5)          
        type(Key.TAB)
        type('valhalla.telerikacademy@gmail.com')
        type(Key.TAB)
        type('Valhalla#1')
        type(Key.ENTER)  
        wait(MainPageNavBar.Settings,5) 
        assert(exists(MainPageNavBar.Settings).highlight(2))

    def test_004_GetListOfAllnationalities(self):
        wait(MainPageNavBar.Settings,5)
        click(MainPageNavBar.Settings)
        wait(Nationalities.Nationalities,5) 
        click(Nationalities.Nationalities)        
        assert(exists(Nationalities.AddNationality).highlight(2))

    def test_005_CreateNationality(self):  
        wait(Nationalities.AddNationality,5)       
        click(Nationalities.AddNationality)
        wait(Nationalities.TypeNationality,5)   
        click(Nationalities.TypeNationality)  
        wait(5)   
        type('Valhalla' + Key.ENTER)         
        assert(exists(Nationalities.AddNationality).highlight(2)) 

    def test_006_EditNationality(self): 
        wait(5)   
        type(Key.END)   
        wait(Pattern(Nationalities.EditVal).similar(0.83).targetOffset(379,0),5)        
        click(Pattern(Nationalities.EditVal).similar(0.83).targetOffset(379,0))
        wait(Nationalities.TypeNationality,5)   
        click(Nationalities.TypeNationality)  
        wait(5)   
        type('a', Key.CTRL + 'VALHALLA' + Key.ENTER)        
        assert(exists(Nationalities.AddNationality).highlight(2))      

    def test_007_DeleteNationality(self):
        wait(5)   
        type(Key.END)   
        wait(Pattern(Nationalities.EditValhalla).similar(0.71).targetOffset(458,0),5)     
        click(Pattern(Nationalities.EditValhalla).similar(0.71).targetOffset(458,0))
        wait(Nationalities.Delete,5)   
        click(Nationalities.Delete)
        wait(Nationalities.Yes,5)   
        click(Nationalities.Yes)        
        assert(exists(MainPageNavBar.Settings).highlight(2))
              
    def test_008_SearchPost(self):
        wait(MainPageNavBar.Settings,5)
        click(MainPageNavBar.Settings)
        wait(Pattern(Posts.SearchPost).targetOffset(-30,16),5)
        click(Pattern(Posts.SearchPost).targetOffset(-30,16))
        wait(Posts.SearchFieldPost,5)
        click(Posts.SearchFieldPost)
        type('Slavia Sofia') 
        click(Posts.SearchButton)
        wait(Posts.Slavia,5)
        assert(exists(Posts.Slavia).highlight(2)) 

    def test_009_EditPost(self):
        click(Posts.SlaviaPostTitle,5)
        wait(5) 
        type(Key.END)
        wait(Posts.EditPost,5)
        click(Posts.EditPost)
        wait(5) 
        type(Key.END)
        wait(Pattern(Posts.Description).targetOffset(79,0),5)
        click(Pattern(Posts.Description).targetOffset(79,0))
        type(' There are no matches arranged in Bulgaria!')
        click(Posts.SavePost)
        wait(5) 
        type(Key.END)
        assert(exists(Posts.PostExists).highlight(2)) 

    def test_010_DeletePost(self):
        type(Key.PAGE_UP)
        wait(MainPageNavBar.Settings,5)
        click(MainPageNavBar.Settings)
        wait(Pattern(Posts.SearchPost).targetOffset(-30,16),5)
        click(Pattern(Posts.SearchPost).targetOffset(-30,16))
        wait(Posts.SearchFieldPost,5)
        click(Posts.SearchFieldPost)
        type('Something to be deleted') 
        click(Posts.SearchButton)
        wait(Posts.PostToDelete,5)
        click(Posts.PostTitleToDelete,5)
        wait(5) 
        type(Key.END)
        wait(Posts.EditPost,5)
        click(Posts.EditPost)
        wait(5) 
        type(Key.END)
        wait(Posts.DeletePost,5) 
        click(Posts.DeletePost)
        wait(Posts.NoDelete,5) 
        click(Posts.NoDelete)         
        assert(exists(Posts.PostToDelete)

    def test_011_GetListOfAllcategories(self):
        wait(MainPageNavBar.Settings,5)
        click(MainPageNavBar.Settings)
        wait(MainPageNavBar.Categories,5) 
        click(MainPageNavBar.Categories)
        wait(Categories.AddCategory,5)
        assert(exists(Categories.AddCategory).highlight(2))

    def test_012_CreateCategory(self):
        click(Categories.AddCategory)        
        wait(Categories.SelectFile,5)
        click(Categories.SelectFile) 
        sleep(5) 
        click(Categories.Levski)      
        wait(Categories.Open,5)
        click(Categories.Open)
        type(Key.TAB)
        type('LEVSKI' + Key.ENTER)       
        assert(exists(Categories.AddCategory).highlight(2))

    def test_013_CreateCategoryWithAlreadyExistingName(self):
        click(SocialNetworkUI.AddCategory)
        wait(SocialNetworkUI.SelectFile,5) 
        click(SocialNetworkUI.SelectFile)
        wait(SocialNetworkUI.Levski,5) 
        click(SocialNetworkUI.Levski)
        wait(SocialNetworkUI.Open,5) 
        click(SocialNetworkUI.Open)
        wait(5) 
        type(Key.TAB)
        type('LEVSKI' + Key.ENTER)
        wait(SocialNetworkUI.Error,5) 
        click(SocialNetworkUI.Error)
        assert(exists().highlight(2)) 

    def test_014_DeleteCategory(self):
        wait(SocialNetworkUI.Settings,5) 
        click(SocialNetworkUI.Settings)
        wait(SocialNetworkUI.Categories,5) 
        click(SocialNetworkUI.Categories)
        wait(5) 
        type(Key.END)   
        wait(Pattern(SocialNetworkUI.Edit).similar(0.50).targetOffset(480,0),5)       
        click(Pattern(SocialNetworkUI.Edit).similar(0.50).targetOffset(480,0))
        wait(SocialNetworkUI.Delete,5) 
        click(SocialNetworkUI.Delete)
        wait(SocialNetworkUI.Yes,5) 
        click(SocialNetworkUI.Yes)         
        assert(exists().highlight(2))		    

    def test_015_CloseWindows(self):
        SocialNetwork.Close()        

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='Admin Tests Report' )
    runner.run(suite)
    outfile.close()